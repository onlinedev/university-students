import Taro from '@tarojs/taro'
import dva from './utils/dva'

import scoreModel from './pages/score/model'
import settingModel from './pages/me/model'
import loginModel from './pages/login/model'
import courseModel from './pages/schedule/model'

export default dva.createApp({
  initialState: {},
  models: [ scoreModel, loginModel, settingModel, courseModel ],
});
