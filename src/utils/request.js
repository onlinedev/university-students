import Taro from '@tarojs/taro'
import login from './login'

const config = {
  baseUrl: 'https://hfutonline.natapp4.cc',
  // baseUrl: 'https://hfut.lemonlife.top',
  // baseUrl: 'http://192.168.0.121:8088',
}

const baseOptions = ({ url, data, method }) => Taro.getStorage({ key: 'token' })
.then(storage => storage.data)
.catch(() => login(config))
.then(token => Taro.request({
  isShowLoading: false,
  url: config.baseUrl + url,
  data: data,
  method: method,
  header: {
    'content-type': 'application/x-www-form-urlencoded',
    'token': token,
  },
  fail: () => {
    Taro.showToast({
      icon: "none",
      title: "网络错误，请稍后再试",
    });
  }
}))
.then(res => {
  if (res.statusCode !== 200) {
    Taro.showToast({
      icon: "none",
      title: "网络错误，请稍后再试",
    });
    throw new Error("网络错误");
  }
  return res.data;
})
.then(resData => {
  const { code, msg } = resData
  if (code === 1001 || code === 1002 || code === 1003 || code === 1004 || code === 1005) {
    return login(config)
    .then(() => baseOptions({ url, data, method }))
  }
  if (code !== 2000) {
    Taro.showToast({
      icon: "none",
      // title: msg || resData.message || "网络繁忙，请稍后再试",
      title: "网络繁忙，请稍后再试",
    });
    Taro.navigateTo({url:'/pages/error/index'})
  }
  return resData;
})
.catch(e => {
  console.error(e)
  Taro.navigateTo({url:'/pages/error/index'})
})

export const get = (url, data = '') => baseOptions({
  url,
  data,
  method: 'GET'
})

export const post = (url, data = {}) => baseOptions({
  url,
  data,
  method: 'POST'
})

export const DELETE = (url, data = {}) => baseOptions({
  url,
  data,
  method: 'DELETE'
})

export const PUT = (url, data = {}) => baseOptions({
  url,
  data,
  method: 'PUT'
})

export const json = (url, method, data, token) => {
  return Taro.request({
    isShowLoading: false,
    url: config.baseUrl + url,
    data: data,
    method: method,
    header: {
      'content-type': 'application/json;charset=UTF-8',
      'token': token,
    },
    fail: () => {
      Taro.showToast({
        icon: "none",
        title: "网络错误，请稍后再试",
      });
    }
  })
  .then(res => {
    if (res.statusCode !== 200) {
      Taro.showToast({
        icon: "none",
        title: "网络错误，请稍后再试",
      });
      throw new Error("网络错误");
    }
    return res.data;
  })
}