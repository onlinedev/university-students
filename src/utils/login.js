import Taro from '@tarojs/taro'

export default ({ baseUrl }) => Taro.login()
.then(({ code }) => Taro.request({
  isShowLoading: false,
  url: `${baseUrl}/auth/login`,
  data: {
    code
  },
  method: 'POST',
  header: {
    'content-type': 'application/x-www-form-urlencoded',
  },
  fail: () => {
    Taro.showToast({
      icon: "none",
      title: "网络错误，请稍后再试",
    });
  }
}))
.then(res => {
  if (res.statusCode !== 200) {
    Taro.showToast({
      icon: "none",
      title: "网络错误，请稍后再试",
    });
    throw new Error("网络错误");
  }
  return res.data;
})
.then(({code, data, msg}) => {
  const { state, token} = data || {};
  if( code !== 2000){
    throw new Error(msg)
  }

  // 登录成功，存储 token
  token && Taro.setStorage({
    key: "token",
    data: token
  })

  // 未绑定账号，终止 promise
  let user = Taro.getStorageSync('loginFormData')
  if (state && !user) {
    Taro.redirectTo({
      url: '/pages/login/index'
    })
    Taro.showToast({
      icon: "none",
      title: '该微信未绑定账号',
    });
    throw new Error('该微信未绑定账号')
  }

  // 已绑定账号时，返回 token
  return token
})

