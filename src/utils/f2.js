import Taro from '@tarojs/taro'

const strLen = (str) => {
  let len = 0;
  for (let i = 0; i < str.length; i++) {
    if (str.charCodeAt(i) > 0 && str.charCodeAt(i) < 128) {
      len++;
    } else {
      len += 2;
    }
  }

  return len;
}

export const measureText = (text, font, ctx) => {
  if (!ctx) {
    let fontSize = 12;
    if (font) {
      fontSize = parseInt(font.split(' ')[3], 10);
    }
    fontSize /= 2;
    return {
      width: strLen(text) * fontSize
    };
  }

  ctx.font = font || '12px sans-serif';
  return ctx.measureText(text);
};

export const fixF2 = (FF) => {
  if (process.env.TARO_ENV !== 'h5') {
    FF.Util.measureText = function (text, font, ctx) {
      if (!ctx) {
        let fontSize = 12;
        if (font) {
          fontSize = parseInt(font.split(' ')[3], 10);
        }
        fontSize /= 2;
        return {
          width: strLen(text) * fontSize
        };
      }
      ctx.font = font || '12px sans-serif';
      return ctx.measureText(text);
    };
    FF.Util.addEventListener = function (source, type, listener) {
      source.addListener(type, listener);
    };
    FF.Util.getStyle = function (el, property) {
      return el.currentStyle ? el.currentStyle[property] : undefined;
    };
    FF.Util.removeEventListener = function (source, type, listener) {
      source.removeListener(type, listener);
    };
    FF.Util.createEvent = function (event, chart) {
      const type = event.type;
      let x = 0;
      let y = 0;
      const touches = event.touches;
      if (touches && touches.length > 0) {
        x = touches[0].x;
        y = touches[0].y;
      }
      return {
        type,
        chart,
        x,
        y
      };
    };
    if (Taro && Taro.getSystemInfoSync) {
      const systeamInfo = Taro.getSystemInfoSync();
      if (systeamInfo && systeamInfo.pixelRatio) {
        FF.Global.pixelRatio = systeamInfo.pixelRatio
      }
    }
  } else {
    FF.Global.pixelRatio = window.devicePixelRatio
  }
  FF.TaroFixed = true;
  return FF
}
