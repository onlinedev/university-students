/* eslint-disable */

Component({
  data: {
    selected: 0,
    color: "#666",
    backgroundColor: '#fff',
    selectedColor: "#217aff",
    list: [{
      pagePath: '/pages/schedule/index',
      text: '课表',
      iconPath: '/assets/schedule.png',
      selectedIconPath: '/assets/schedule_full.png',
    }, {
      pagePath: '/pages/score/index',
      text: '成绩',
      iconPath: '/assets/score.png',
      selectedIconPath: '/assets/score_full.png',
    }, {
      pagePath: '/pages/me/index',
      text: '我的',
      iconPath: '/assets/me.png',
      selectedIconPath: '/assets/me_full.png',
    }]
  },
  attached() {
  },
  methods: {
    switchTab(e) {
      const data = e.currentTarget.dataset
      const url = data.path
      wx.switchTab({url})
      // this.setData({
      //   selected: data.index
      // })
    }
  }
})
