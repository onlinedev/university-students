import "@tarojs/async-await";
import Taro, { Component } from '@tarojs/taro'
import { Provider } from "@tarojs/redux";

import Entry from './pages/entry/index'
import dvaApp from './dva-app'

import './style/app.scss'
import './style/font.scss'

const store = dvaApp.getStore();

class App extends Component {
  
  config = {
    pages: [
      'pages/schedule/index',
      'pages/score/index',
      'pages/me/index',
      'pages/error/index',
      'pages/login/index',
      'pages/login/components/advanced-login/index',
      'pages/login/components/user-agreement/index',
      'pages/login/components/prepare-data/index',
      'pages/login/components/get-user-info/index',
      'pages/schedule/components/item-detail/index',
      // 'pages/score/components/student-evaluation/index',
      'pages/score/components/score-compute/index',
      'pages/score/components/score-analysis/components/election-list/index',
      'pages/score/components/score-detail/index',
      'pages/score/components/score-evaluate/index',
      'pages/score/components/score-analysis/components/score-target/set-score-target',
      'pages/me/components/feedback/index',
      'pages/me/components/setting/index',
      'pages/me/components/help/index',
      'pages/me/components/about/index',
      'pages/me/components/join-us/index',
    ],
    window: {
      backgroundTextStyle: 'light',
      backgroundColor: '#fff',
      navigationBarBackgroundColor: '#fff',
      navigationBarTitleText: 'HFUTonline',
      navigationBarTextStyle: 'black',
    },
    tabBar: {
      color: "#666",
      selectedColor: "#217aff",
      backgroundColor: '#fff',
      borderStyle: 'white',
      custom: true,
      list: [{
        pagePath: 'pages/schedule/index',
        text: '课表',
        iconPath: 'assets/schedule.png',
        selectedIconPath: 'assets/schedule_full.png',
      }, {
        pagePath: 'pages/score/index',
        text: '成绩',
        iconPath: 'assets/score.png',
        selectedIconPath: 'assets/score_full.png',
      }, {
        pagePath: 'pages/me/index',
        text: '我的',
        iconPath: 'assets/me.png',
        selectedIconPath: 'assets/me_full.png',
      }]
    }
  }

  componentWillMount() {
  }

  render () {
    return (
      <Provider store={store}>
        <Entry />
      </Provider>
    )
  }
}

Taro.render(<App />, document.getElementById('app'))
