import Taro, { Component } from '@tarojs/taro'
import { View, Text, Image } from '@tarojs/components'

import { CustomButton } from '../../../../components/custom-button'

import './index.scss'

export default class JoinUs extends Component {

  config = {
    navigationBarTitleText: '加入我们',
    navigationBarTextStyle: 'black',
    navigationBarBackgroundColor: '#fff',
  }

  handleJoinUs = () => {
    Taro.setClipboardData({
      data: 'https://v2.oa.hfutonline.net/#/add',
      success: function () {
        Taro.hideToast();
        Taro.showModal({
          title: '报名链接已复制',
          showCancel: false,
          content: '报名链接已复制到剪贴板，请前往浏览器里粘贴打开',
        })
      }
    })
  }

  render() {
    const data = [
      {
        image: 'http://139.198.15.213:10086/hfutonline/cp.png',
        value: '产品部'
      }, {
        image: 'http://139.198.15.213:10086/hfutonline/sjsj.png',
        value: '视觉设计部'
      }, {
        image: 'http://139.198.15.213:10086/hfutonline/js.png',
        value: '技术部'
      }, {
        image: 'http://139.198.15.213:10086/hfutonline/yytg.png',
        value: '运营推广部'
      }, {
        image: 'http://139.198.15.213:10086/hfutonline/sp.png',
        value: '视频部'
      }, {
        image: 'http://139.198.15.213:10086/hfutonline/cpkf.png',
        value: '产品开发部'
      }, {
        image: 'http://139.198.15.213:10086/hfutonline/yy.png',
        value: '运营部'
      }, {
        image: 'http://139.198.15.213:10086/hfutonline/bgs.png',
        value: '办公室'
      }
    ]

    return (
      <View className='join'>
        <View className='join-header full-card'>
          <Image className='join-header-img'
            src='http://139.198.15.213:10086/hfutonline/mlylogo.png'
            mode='aspectFit'
          />
          <Text className='join-header-name'>“明理苑”大学生网络文化工作室</Text>
          <Text className='join-header-text'>立德明理 善事利器</Text>
        </View>

        <View className='join-content full-card'>
            {
              data.map(item =>
                <View className='join-content-item' key={item}>
                  <Image className='join-content-item-img' src={item.image} />
                  <View className='join-content-item-text'>{item.value}</View>
                </View>
              )
            }
        </View>

        <CustomButton
          value='点击报名'
          isFixed
          type='primary'
          onSubmit={this.handleJoinUs}
        />
      </View>
    )
  }
}