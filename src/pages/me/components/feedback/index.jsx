import Taro, { Component } from '@tarojs/taro'
import { View, Textarea, Input } from '@tarojs/components'
import { AtIcon, AtTag } from 'taro-ui'
import { connect } from '@tarojs/redux'

import CustomButton from '../../../../components/custom-button'

import './index.scss'

@connect(({ me }) => ({ me }))
export default class ScoreDetail extends Component {

  config = {
    navigationBarTitleText: '吐槽',
    navigationBarTextStyle: 'black',
    navigationBarBackgroundColor: '#fff',
  }

  componentDidMount() {
  }

  handleChange = (type, event) => {
    let value;
    if(type=='feedbackTag'){
      value = event.name
    } else {
      value = event.target.value
    }

    this.props.dispatch({
      type: 'me/setFeedbackData',
      payload: {[type]: value}
    })
  }

  onSubmit = () => {
    const { currentTag, qq, content } = this.state

    this.props.dispatch({
      type: 'me/feedback',
      payload: {
        qq,
        content,
        type: currentTag,
      }
    })
  }

  getQQnumber = () => {
    Taro.setClipboardData({
      data: '995295728',
      success: function () {
        Taro.hideToast();
        Taro.showModal({
          title: '小提示',
          showCancel: false,
          content: '群号码已复制，请您前往qq粘贴搜索',
        })
      }
    })
  }

  render() {
    const { me = {} } = this.props;
    const { isFeedbackLoading, isFeedbackDisabled } = me.uiData;
    const { feedbackContent, feedbackTag, feedbackQQ } = me.bizData;

    return (
      <View className='feedback'>
        <View className='feedback-card full-card'>
          <View className='feedback-card-header'>问题分类</View>
          <View className='feedback-card-content'>
            <AtTag
              className='feedback-card-content-tag'
              type='primary'
              name='1'
              active={feedbackTag == '1'}
              onClick={this.handleChange.bind(this, 'feedbackTag')}
            >
              账号问题
              </AtTag>
            <AtTag
              className='feedback-card-content-tag'
              type='primary'
              name='2'
              active={feedbackTag == '2'}
              onClick={this.handleChange.bind(this, 'feedbackTag')}
            >
              数据问题
              </AtTag>
            <AtTag
              className='feedback-card-content-tag'
              type='primary'
              name='3'
              active={feedbackTag == '3'}
              onClick={this.handleChange.bind(this, 'feedbackTag')}
            >
              其他问题
              </AtTag>
            <AtTag
              className='feedback-card-content-tag'
              type='primary'
              name='4'
              active={feedbackTag == '4'}
              onClick={this.handleChange.bind(this, 'feedbackTag')}
            >
              意见建议
              </AtTag>
          </View>
        </View>

        <View className='feedback-card full-card'>
          <View className='feedback-card-header'>吐槽内容</View>
          <View className='feedback-card-content'>
            <Textarea
              className='feedback-card-content-input'
              count={false}
              value={feedbackContent}
              onInput={this.handleChange.bind(this, 'feedbackContent')}
              maxLength={100}
              style='height:150rpx;'
              placeholder={feedbackTag == 4 ? '请简要描述您的意见' : '请简要描述问题并提供必要的个人信息'}
              placeholderStyle='color:#ccc;'
            />
          </View>
        </View>

        <View className='feedback-card full-card'>
          <View className='feedback-card-header'>联系方式</View>
          <View className='feedback-card-content'>
            <Input
              className='feedback-card-content-input'
              type='number'
              value={feedbackQQ}
              onInput={this.handleChange.bind(this, 'feedbackQQ')}
              maxLength={11}
              placeholder='请留下你的QQ号（选填）'
              placeholderStyle='color:#ccc;'
            />
          </View>
        </View>

        <View className='feedback-card full-card'>
          <View className='feedback-card-header'>你也可以</View>
          <View className='feedback-card-content'>
            <View
              className='feedback-card-content-link'
              onClick={this.getQQnumber}
            >
              加入官方反馈群
              <AtIcon prefixClass='iconfont' value='right' size='16' color='#576b95' />
            </View>
          </View>
        </View>

        <CustomButton
          value='提交'
          type='primary'
          disabled={isFeedbackDisabled}
          loading={isFeedbackLoading}
          onSubmit={this.onSubmit}
        />
      </View>
    )
  }
}
