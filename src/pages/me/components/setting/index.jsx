import Taro, { Component } from '@tarojs/taro'
import { View } from '@tarojs/components'
import { AtSwitch, AtTag } from 'taro-ui'
import { connect } from '@tarojs/redux'

import CustomButton from '../../../../components/custom-button'

import './index.scss'

@connect(({ me }) => ({ me }))
export default class Scoretarget extends Component {

  config = {
    navigationBarTitleText: '设置',
    navigationBarTextStyle: 'black',
    navigationBarBackgroundColor: '#fff',
  }

  componentDidMount() {
  }

  handleBtnClick = () => {
    Taro.showModal({
      title: '确认注销登录？',
      content: '注销登录后个人信息会丢失，是否继续',
    })
      .then(res => {
        if (res.confirm) {
          this.props.dispatch({
            type: 'me/quit',
          })
        }
      })
  }

  refreshMajor = () => {
    const { me } = this.props;
    const { updateTimeSpan } = me.bizData;
    const timeSpan = Taro.getStorageSync('refreshMajorTime');
    let minutes = (new Date().getHours() - timeSpan[0]) * 60 + new Date().getMinutes() - timeSpan[1];
    if (timeSpan && timeSpan[2]==new Date().getDate() && minutes < updateTimeSpan) {
      Taro.showModal({
        title: '提示',
        content: `先去喝杯咖啡吧，${updateTimeSpan-minutes}分钟后再来试试`,
        showCancel: false
      })
    } else {
        this.props.dispatch({
          type: 'me/freshMajor',
        })
    }
  }

  switchChange = (type, value) => {
    this.props.dispatch({
      type: 'me/settingChange',
      payload: { [type]: value ? 1 : 0 }
    })
  }

  render() {
    const { me = {} } = this.props;
    const { avgScoreType, gradePointType } = me.bizData;
    const {
      isAvgScoreLoading,
      isGradePointLoading,
      isRefreshLoading,
      isQuitLoading,
      isSettingDisabled,
    } = me.uiData;

    return (
      <View className='setting'>
        <View className='setting-card full-card'>
          <View className='setting-card-title'>成绩设置</View>
          <View className='setting-card-item'>
            <AtSwitch className='setting-card-item-switch'
              title='均分包含公选课'
              color='#217aff'
              loading={isAvgScoreLoading}
              disabled={isSettingDisabled}
              checked={avgScoreType ? true : false}
              onChange={this.switchChange.bind(this, 'avgScoreType')}
            />
          </View>

          <View className='setting-card-item'>
            <AtSwitch className='setting-card-item-switch'
              title='绩点包含公选课'
              color='#217aff'
              loading={isGradePointLoading}
              disabled={isSettingDisabled}
              checked={gradePointType ? true : false}
              onChange={this.switchChange.bind(this, 'gradePointType')}
            />
          </View>

          <View className='setting-card-item'>
            <View className='setting-card-item-picker'>
              <View className='setting-card-item-picker-label'>排名计算方式</View>
              <View className='setting-card-item-picker-value'>
                <AtTag
                  className='setting-card-item-picker-value-left'
                  name='0'
                  active={me.bizData.rankType == '0'}
                  onClick={this.switchChange.bind(this, 'rankType', '')}
                >
                  均分
                  </AtTag>
                <AtTag
                  className='setting-card-item-picker-value-right'
                  name='1'
                  active={me.bizData.rankType == '1'}
                  onClick={this.switchChange.bind(this, 'rankType', '1')}
                >
                  绩点
                  </AtTag>
              </View>
            </View>
          </View>
        </View>

        {/* <Picker mode='selector'
          range={me.bizData.selectorMajor}
          value={me.bizData.selectorMajorValue}
          disabled={isSelectorMajorLoading}
          onChange={this.handlePickerChange.bind(this)}
        >
        </Picker> */}

        <CustomButton
          value='更新专业信息'
          isFull
          type='default'
          loading={isRefreshLoading}
          disabled={isSettingDisabled}
          onSubmit={this.refreshMajor}
        />

        <CustomButton
          value='注销登录'
          isFull
          type='danger'
          loading={isQuitLoading}
          disabled={isSettingDisabled}
          onSubmit={this.handleBtnClick}
        />
      </View>
    )
  }
}
