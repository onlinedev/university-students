import Taro, { Component } from '@tarojs/taro'
import { View, Text } from '@tarojs/components'
// import { AtList, AtListItem, AtAccordion } from "taro-ui"
import { connect } from '@tarojs/redux'

import './index.scss'

@connect(({ me }) => ({ me }))
export default class ScoreDetail extends Component {

  config = {
    navigationBarTitleText: '帮助',
    navigationBarTextStyle: 'black', //white
    navigationBarBackgroundColor: '#fff', //#1890ff
  }

  componentDidMount(){
    this.props.dispatch({
      type: 'me/getQuestion'
    })
  }

  handelClick = type => {
    Taro.showToast({title: type, icon: 'none',duration: 2000})
  }

  render() {
    const { me={} } = this.props;
    const { questionForLogin=[], questionForScore=[], questionForOther=[] } = me.bizData;
    
    return (
      <View className='help'>
        <View className='help-card'>
          <View className='help-card-header'>登录问题</View>
          <View className='help-card-content'>
            {questionForLogin.map(item=><View className='help-card-content-text' key={item.q}>
              <Text className='help-card-content-text-q'>问：{item.q+`\n`}</Text>
              <Text className='help-card-content-text-a'>答：{item.a}</Text>
               </View>
            )}
          </View>
        </View>
        <View className='help-card'>
          <View className='help-card-header'>成绩问题</View>
          <View className='help-card-content'>
            {questionForScore.map(item=><View className='help-card-content-text' key={item.q}>
                <Text className='help-card-content-text-q'>问：{item.q+`\n`}</Text>
                <Text className='help-card-content-text-a'>答：{item.a}</Text>
                </View>
              )}
          </View>
        </View>
        <View className='help-card'>
          <View className='help-card-header'>其他问题</View>
          <View className='help-card-content'>
            {questionForOther.map(item=><View className='help-card-content-text' key={item.q}>
                <Text className='help-card-content-text-q'>问：{item.q+`\n`}</Text>
                <Text className='help-card-content-text-a'>答：{item.a}</Text>
                </View>
              )}
            <View className='help-card-content-text'>
              <Text className='help-card-content-text-q'>问：我也想加入你们的团队，请问什么时候招新呢？{`\n`}</Text>
              <View className='help-card-content-text-a'
                onClick={()=>{
                Taro.setClipboardData({
                  data: 'https://v2.oa.hfutonline.net/#/add',
                  success: function() {
                    Taro.hideToast();
                    // Taro.showToast({title: 'type', icon: 'none',duration: 2})
                    Taro.showModal({
                      title: '小提示',
                      showCancel:false,
                      content: '链接地址已复制，请您到浏览器里粘贴打开',
                      success: function() {}
                      })}
                })}}
              >答：“明理苑”大学生网络文化工作室一般于每学期初进行正式招新，具体可关注公众号微言合工大。开发团队招新链接在非正式招新时间也向大家开放，大家可以点击<Text style={{color:'#576b95'}}>招新链接</Text>进行报名。</View>
            </View>
            <View className='help-card-content-text'>
              <Text className='help-card-content-text-q'>问：我有问题想吐槽/我有建议想反馈，我应该怎么做？{`\n`}</Text>
              <View className='help-card-content-text-a'
                onClick={()=>{
                Taro.setClipboardData({
                  data: '995295728',
                  success: function() {
                    Taro.hideToast();
                    // Taro.showToast({title: 'type', icon: 'none',duration: 2})
                    Taro.showModal({
                      title: '小提示',
                      showCancel:false,
                      content: '群号码已复制，请您前往QQ粘贴搜索',
                      success: function() {}
                      })}
                })}}
              >答：可以通过“我的-吐槽”向我们反馈或是加入<Text style={{color:'#576b95'}}>官方反馈QQ群</Text>与我们进行更多交流。</View>
            </View>
          </View>
        </View>
        {/* <View className='help-footer' onClick={this.handelClick.bind(this,'点击加入')}>加入HFUTonline官方反馈QQ群</View> */}
      </View>
    )
  }
}
