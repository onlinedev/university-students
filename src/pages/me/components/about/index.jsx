import Taro, { Component } from '@tarojs/taro'
import { View, Text, Image, Button } from '@tarojs/components'
import { AtIcon } from "taro-ui"

import './index.scss'

export default class About extends Component {

  config = {
    navigationBarTitleText: '关于',
    navigationBarTextStyle: 'black',
    navigationBarBackgroundColor: '#fff',
  }

  state = {
    us: '本产品由“明理苑”大学生网络文化工作室独立开发。“明理苑”隶属于教育部，由合肥工业大学党委学工部管辖，是首批“教育部大学生网络文化工作室”之一。如果你对互联网产品开发、新媒体运营感兴趣，欢迎点击下方链接加入我们。'
  }

  handleWechat = () => {
    Taro.setClipboardData({
      data: '微言合工大',
      success: function () {
        Taro.hideToast();
        Taro.showModal({
          title: '微信公众号已复制',
          showCancel: false,
          content: '微信公众号已复制到剪贴板，请前往微信粘贴搜索',
        })
      }
    })
  }

  onShareAppMessage = () => ({
    title: 'HFUTonline',
    imageUrl: 'http://139.198.15.213:10086/hfutonline/share.png'
  })

  joinUsPage = () => {
    Taro.navigateTo({
      url: '/pages/me/components/join-us/index',
    })
  }

  agreementPage = () => {
    Taro.navigateTo({
      url: '/pages/login/components/user-agreement/index',
    })
  }

  render() {
    return (
      <View className='about'>
        <View className='about-header full-card'>
          <Image className='about-header-img'
            src='http://139.198.15.213:10086/hfutonline/logo.png'
            mode='aspectFit'
          />
          <Text className='about-header-name'>{`HFUTonline ${this.$router.params.version}`}</Text>
          <Text className='about-header-text'>工大师生一站式服务平台</Text>
          <View className='about-header-link'>
            <View className='about-header-link-wechat'
              onClick={this.handleWechat}
            >
              关注公众号
            </View>
            <Text className='about-header-link-line'>|</Text>
            <Button className='about-header-link-share'
              open-type='share'
            >
              分享给好友
            </Button>
          </View>
        </View>

        <View className='about-card full-card'>
          <View className='about-card-header'>关于我们</View>
          <View className='about-card-content'>{this.state.us}</View>
          <View className='about-card-link'
            onClick={this.joinUsPage}
          >
            加入我们
              <AtIcon prefixClass='iconfont' value='right' size='16' color='#576b95' />
          </View>
        </View>

        <View className='about-footer footer'
          onClick={this.agreementPage}
        >
          《用户协议》
        </View>
      </View>
    )
  }
}
