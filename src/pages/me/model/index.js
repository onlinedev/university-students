import Taro from '@tarojs/taro'
import { post, get, DELETE } from '../../../utils/request'
import hfutonline from '../../../../package.json'

export default {
  namespace: 'me',
  state: {
    bizData: {
      version: hfutonline.version,
      feedbackContent: '',
      feedbackTag: '',
      feedbackQQ: '',
      updateTimeSpan: 30,
      avgScoreType: 0,
      gradePointType: 0,
      rankType: 0,
      // selectorMajor: ['道桥', '地下', '工民建', '工管'],
      // selectorMajorValue: 0,
      questionForLogin:[
        {q:'我一定要授权获取微信账号信息吗？',a:'是的，小程序需要授权微信登录后才可使用。'},
        {q:'我一定要绑定教务系统账号吗？',a:'是的，若不绑定教务系统账号，小程序内主要功能将无法使用。您的教务系统账号信息将只用于数据的获取，我们承诺保护您的隐私。您也可以随时在个人设置页面解除绑定。'},
      ],
      questionForScore:[
        {q:'成绩的数据来源是什么？',a:'数据来自学校的教务系统。'},
        {q:'平均成绩是怎么计算的？排名是怎么计算的？',a:'平均成绩包括平均绩点与平均分数，默认是不含公选的全部课程成绩的均值。您可以在个人设置页面对是否包含公选成绩进行设置。排名是根据平均分数进行统计的。'},
        {q:'成绩排名准确吗？',a:'本小程序的成绩排名并非学校官方排名，是由已绑定用户的成绩数据为依据进行的排名，即排名总人数为当前该专业的用户数。所以用户越多，排名越准确哦，快去推荐给你身边的同学们吧。'},
        {q:'公选学分统计准确吗？',a:'该学分分布仅供参考，具体政策见各学院文件。'},
      ],
      questionForOther:[
        {q:'我可以选择某些成绩下载吗？',a:'“成绩下载”功能目前仅支持全部成绩的导出。'},
        {q:'“成绩分享”是什么？分享后可以撤销吗？',a:'“成绩分享”可以向他人展示该科成绩，点击分享后会显示您的微信号与成绩，并按照分数进行排序，同时您也可以看到其他用户分享的成绩信息。分享后是不可以撤销的。'},
      ],
    },
    uiData: {
      // 开关loading
      isAvgScoreLoading: false,
      isGradePointLoading: false,
      // 按钮loading
      isRefreshLoading: false,
      isQuitLoading: false,
      isFeedbackLoading: false,
      // 设置页disabled
      isSettingDisabled: false,
      // 反馈页disabled
      isFeedbackDisabled: true,
      // 设置变更
      isSettingChange: false,
    },
  },
  subscriptions: { },
  effects: {
    // * prepareData(_, { call }) {
    // }
    * getSetting({}, {call,put}){
      try {
        const { success, data } = yield call(get, '/api/setting')
        if(success){
          yield put({
            type: 'setBizData',
            payload:data
          })
        }
      } catch(e){
        console.log(e);
      }
    },
    * settingChange({payload},{put,call}){
      try {
        yield put({
          type: 'setUiData',
          payload:{
            isAvgScoreLoading: true,
            isGradePointLoading: true,
            isSettingDisabled: true
          }
        })
        const { success, data } = yield call(post, '/api/setting', payload)
        if(success){
          yield put({
            type: 'setBizData',
            payload:data
          })
          yield put({
            type: 'setUiData',
            payload:{isSettingChange: true}
          })
          Taro.showToast({
            title: '修改成功',
            icon: 'success',
            duration: 2000
          })
        } else {
          Taro.showToast({
            title: String(data),
            icon: 'none',
            duration: 2000
          })
        }
      } catch(e) {
        console.log(e);       
      } finally {
        yield put({
          type: 'setBizData',
          payload
        })
        yield put({
          type: 'setUiData',
          payload:{
            isAvgScoreLoading: false,
            isGradePointLoading: false,
            isSettingDisabled: false
          }
        })
        // console.log();
      }
    },
    * freshMajor({payload},{put,call}){
      console.log(payload);
      try { 
        yield put({
          type: 'setUiData',
          payload:{
            isRefreshLoading: true,
            isSettingDisabled: true
          }
        })
        // const { success, data } = yield call(DELETE, '/api/quit');
        const success = yield call(post, '/api/user/profession/update');
        if(success){
          Taro.showToast({
            title: '更新成功',
            icon: 'success',
            duration: 2000
          })
          Taro.removeStorageSync('token')

          yield put({
            type: 'score/getScoreAnalysisData',
          })

          Taro.setStorage({
            key: "refreshMajorTime",
            data: [new Date().getHours(), new Date().getMinutes(), new Date().getDate()]
          })
        }
      } catch(e) {
        console.log(e);       
      } finally {
        yield put({
          type: 'setUiData',
          payload:{
            isRefreshLoading: false,
            isSettingDisabled: false
          }
        })
      }
    },
    * quit({payload},{put,call}){
      console.log(payload);
      try { 
        yield put({
          type: 'setUiData',
          payload:{
            isQuitLoading: true,
            isSettingDisabled: true
          }
        })
        // const { success, data } = yield call(DELETE, '/api/quit');
        const success = yield call(DELETE, '/api/user/unBind');
        if(success){
          Taro.showToast({
            title: '解绑成功,谢谢使用',
            icon: 'success',
            duration: 2000
          })

          Taro.getStorageInfo()
          .then(res=> res.keys.map(item=>{
            if(item!='token'){
              Taro.removeStorage({key:item})
            }
          }))
          
          Taro.redirectTo({url:'/pages/login/index'})
        }
      } catch(e) {
        console.log(e);       
      } finally {
        yield put({
          type: 'setUiData',
          payload:{
            isQuitLoading: false,
            isSettingDisabled: false
          }
        })
      }
    },
    * feedback({},{call, put, select}){
      try {

        yield put({
          type: 'setUiData',
          payload:{
            isFeedbackLoading: true,
            isFeedbackDisabled: true
          }
        })

        const { feedbackContent, feedbackTag, feedbackQQ } = yield select(state => state.me.bizData)
        const { brand, model, system, version } = Taro.getSystemInfoSync()
        // console.log(`${payload.content}。来自${systemInfo.brand+systemInfo.model}，系统为${systemInfo.system}，微信版本${systemInfo.version}`);
        // Taro.showToast({
        //       title: `${payload.content}=>来自${systemInfo.brand+systemInfo.model}，系统为${systemInfo.system}，微信版本${systemInfo.version}`,
        //       icon: 'none',
        //       duration: 6000
        //     })

        let payload = {
          qq: feedbackQQ,
          type: feedbackTag,
          content: feedbackContent,
          version: version,
          system: system,
          device: brand+model
        };
        const { success } = yield call(post, '/api/user/feedback' , payload);
        if(success){
          Taro.showModal({
            title: '小提示',
            showCancel:false,
            content: `提交成功，感谢您的反馈`,
            success: function() {
              Taro.navigateBack({ delta: 1 })
            }
          })
        }
        // console.log(payload)
      } catch(e){
        console.log(e);
        
      } finally {
       yield put({
          type: 'setUiData',
          payload:{
            isFeedbackLoading: false,
            isFeedbackDisabled: false,
          }
        })
      }
    },
  },
  reducers: {
    setUiData(state, { payload }){
      return {
        ...state,
        uiData: {
          ...state.uiData,
          ...payload,
        }
      };
    },
    setBizData(state, { payload }){
      return {
        ...state,
        bizData: {
          ...state.bizData,
          ...payload,
        }
      };
    },
    setFeedbackData(state, { payload }){
      // console.log(payload,state)
      return {
        ...state,
        bizData: {
          ...state.bizData,
          ...payload,
        },
        uiData: {
          ...state.uiData,
          isFeedbackDisabled: payload.feedbackContent ? state.bizData.feedbackTag == '': 
          payload.feedbackTag ? state.bizData.feedbackContent =='': state.uiData.isFeedbackDisabled || payload.feedbackContent == ''
        }
      };
    },
  },
};
