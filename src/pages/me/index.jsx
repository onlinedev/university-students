import Taro, { Component } from '@tarojs/taro'
import { View, Text, Image } from '@tarojs/components'
import { connect } from '@tarojs/redux'
import { AtAvatar, AtIcon } from 'taro-ui'

import './index.scss'

@connect(({ score, me, schedule }) => ({ score, me, schedule }))
export default class me extends Component {
  
  config = {
    navigationBarTitleText: '我的',
    navigationBarTextStyle: 'white',
    navigationBarBackgroundColor: '#217aff',
  }

  state = {
    url: {
      help: '/pages/me/components/help/index',
      feedback: '/pages/me/components/feedback/index',
      about: '/pages/me/components/about/index',
      setting: '/pages/me/components/setting/index',
    }
  }

  componentDidShow() {
    try {
      this.$scope.getTabBar().setData({
        selected: 2 // 当前页面对应的 index
      })

    } catch (error) {

    }
    this.props.dispatch({
      type: 'me/getSetting'
    })

    this.props.dispatch({
      type: 'score/getDatastamp'
    })

    this.props.dispatch({
      type: 'schedule/getSchedules'
    })
    
  }

  // 按钮
  handleBtnClick = type => {
    const { url } = this.state;
    const { me = {} } = this.props;
    const { version } = me.bizData;
    Taro.navigateTo({
      url: `${url[type]}?version=${version}`
    })
  }

  render() {
    const { score = {} } = this.props;
    const { userInfo = {}, scoreAnalysis = {} } = score.bizData;
    const {
      avatarUrl = '',
      nickName = '---',
    } = userInfo;
    const { student = {} } = scoreAnalysis
    const {
      sno = '---',
      department = '---'
    } = student;

    return (
      <View className='me'>
        <View className='blue-background'></View>

        <View className='me-header circle-card'>
          <AtAvatar size='large' circle image={avatarUrl}></AtAvatar>
          <View className='me-header-info'>
            <Text className='me-header-info-name'>{nickName}</Text>
            <Text className='me-header-info-num'>学号：{sno} {' ' + department}</Text>
          </View>
        </View>

        <View className='me-card circle-card'>
          <View className='me-card-item' onClick={()=> Taro.showModal({
          title: '提示',
          content: '该功能暂未开放，敬请期待！',
          showCancel: false
        })}
          >
            <AtIcon className='me-card-item-icon' prefixClass='iconfont' value='bell' size='20' color='#333' />
            <Text className='me-card-item-text'>消息</Text>
            <AtIcon prefixClass='iconfont' value='right' size='20' color='#999' />
          </View>
        </View>

        <View className='me-card circle-card'>
          <View className='me-card-item'
            onClick={this.handleBtnClick.bind(this, 'help')}
          >
            <AtIcon className='me-card-item-icon' prefixClass='iconfont' value='question-circle' size='20' color='#333' />
            <Text className='me-card-item-text'>帮助</Text>
            <AtIcon prefixClass='iconfont' value='right' size='20' color='#999' />
          </View>
          <View className='me-card-item'
            onClick={this.handleBtnClick.bind(this, 'feedback')}
          >
            <AtIcon className='me-card-item-icon' prefixClass='iconfont' value='edit-square' size='20' color='#333' />
            <Text className='me-card-item-text'>吐槽</Text>
            <AtIcon prefixClass='iconfont' value='right' size='20' color='#999' />
          </View>
          <View className='me-card-item'
            onClick={this.handleBtnClick.bind(this, 'about')}
          >
            <AtIcon className='me-card-item-icon' prefixClass='iconfont' value='team' size='20' color='#333' />
            <Text className='me-card-item-text'>关于</Text>
            <AtIcon prefixClass='iconfont' value='right' size='20' color='#999' />
          </View>
          <View className='me-card-item'
            onClick={this.handleBtnClick.bind(this, 'setting')}
          >
            <AtIcon className='me-card-item-icon' prefixClass='iconfont' value='setting' size='20' color='#333' />
            <Text className='me-card-item-text'>设置</Text>
            <AtIcon prefixClass='iconfont' value='right' size='20' color='#999' />
          </View>
        </View>

        <View className='me-footer footer'>
          <Image className='me-footer-img'
            src='http://139.198.15.213:10086/hfutonline/mly.png'
            mode='aspectFit'
          />
          <Text className='me-footer-text'>“明理苑”大学生网络文化工作室出品</Text>
        </View>
      </View>
    )
  }
}
