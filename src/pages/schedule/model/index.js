import Taro from '@tarojs/taro'
import { post, get, DELETE } from '../../../utils/request'

export default {
  namespace: 'schedule',
  state: {
    bizData: {
      // 获取新课表时间间隔
      updateTimeSpan: 30,
      colors: [
        ['#ffdbc9', '#bf6c30'],
        ['#ffffc4', '#bfbf30'],
        ['#caffbf', '#48bf30'],
        ['#c9ffed', '#30b3bf'],
        ['#c4ebff', '#3083bf'],
        ['#ccccff', '#7c39bf'],
        ['#ecc7ff', '#a939bf'],
        ['#ffc2db', '#bf397c'],

        ['#ffcccc', '#bf3054'],
        ['#ffecc7', '#bf9b30'],
        ['#e6ffc2', '#a7bf30'],
        ['#bfffcf', '#30bf78'],
        ['#c7ffff', '#308fbf'],
        ['#c2d6ff', '#6639bf'],
        ['#dbc9ff', '#9339bf'],
        ['#ffc4f5', '#bf39bf'],
      ],
      colorForConflict: ['#d8d8d8', '#666666'],
      currentWeek: 1,
      currentClickWeek: 1,
      courseDetail: [],
      weeks: ['', '周一', '周二', '周三', '周四', '周五', '周六', '周日',],
      lessons: ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11'],
      switchWeek: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20']
    },
    uiData: {
      isSkeletonShow: false,
    },
  },
  subscriptions: {},
  effects: {
    // * prepareData(_, { call }) {
    // }
    * getSetting({ }, { call, put }) {
      try {
        const { success, data } = yield call(get, '/api/setting')
        if (success) {
          console.log(data)
          yield put({
            type: 'setBizData',
            payload: data
          })
        }
      } catch (e) {
        console.log(e);
      }
    },
    * updateLesson({ }, { call, put }) {
      Taro.showLoading({ title: '加载中' })
      try {
        const { success } = yield call(post, '/api/schedule/lesson')
        Taro.hideLoading()
        if (success) {
          Taro.removeStorageSync('scheduleData')
          yield put({ type: 'getSchedules' })
          Taro.showToast({
            title: '课表更新成功',
            icon: 'success',
            duration: 2000
          })
          Taro.setStorage({
            key: "updateLesson",
            data: [new Date().getHours(), new Date().getMinutes(), new Date().getDate()]
          })
        } else {
          Taro.showToast({
            title: '课表更新失败',
            icon: 'loading',
            duration: 2000
          })
        }
      } catch (e) {
        console.log(e);
      }
    },
    * getCourseDetail({ payload }, { call, put, select }) {
      try {
        if (payload.scheduleId == '') {
          throw Error(`课程详情数组不能覆盖，每次跳转课程详情时要预先发一个
          this.props.dispatch({
            type: 'schedule/getCourseDetail',
            payload: {scheduleId: ''}
          })
          的预请求清空`)

        }
        yield put({
          type: 'setUiData',
          payload: { isSkeleton: true }
        })
        // 防止多门课由于请求响应时间打乱顺序
        let { courseDetail } = yield select(state => state.schedule.bizData)
        const num = Taro.getStorageSync(`lesson${payload.currentClickWeek}${payload.week}${payload.lesson}`)
        let open = !courseDetail.length && num === 0
        courseDetail.push({ courseName: payload.courseName })
        yield put({
          type: 'setBizData',
          payload: { courseDetail }
        })

        const { success, data } = yield call(get, `/api/schedule/lesson/${payload.scheduleId}`)
        if (success) {
          // console.log(data)
          let oldData = yield select(state => state.schedule.bizData.courseDetail)
          let weekArr = data.weeks.split(',')
          oldData.map(lesson => {
            if (lesson.courseName == payload.courseName) {
              return { ...data, open, weeks: `${weekArr[0]}~${weekArr[weekArr.length - 1]}` }
            } else {
              return lesson
            }
          })
          yield put({
            type: 'setBizData',
            payload: {
              courseDetail: oldData.map(lesson => {
                if (lesson.courseName == payload.courseName) {
                  return { ...data, open, weeks: `${weekArr[0]}~${weekArr[weekArr.length - 1]}` }
                } else {
                  return lesson
                }
              })
            }
          })
        }
        yield put({
          type: 'setUiData',
          payload: { isSkeleton: false }
        })
      } catch (e) {
        console.log(e);
        yield put({
          type: 'setBizData',
          payload: { courseDetail: [] }
        })
      }
    },
    * setCurrentWeek({ payload }, { put }) {
      try {
        // console.log(payload)
        if (payload) {
          yield put({
            type: 'setBizData',
            payload: {
              currentClickWeek: payload
            }
          })
        }
      } catch (e) {
        console.log(e);
      }
    },
    * getSchedules({ payload }, { put, call, select }) {
      // console.log(payload);
      try {
        yield put({
          type: 'setUiData',
          payload: {
            isLoading: true,
          }
        })

        let fromStorage = Taro.getStorageSync('scheduleData')
        if (fromStorage) {
          console.log(fromStorage)
          let currentClickWeek = Math.ceil((new Date().getTime() - new Date(fromStorage.firstDay)) / (3600 * 1000 * 24 * 7));
          let currentWeekChinese = ['', '一', '二', '三', '四', '五', '六', '七', '八', '九', "十", '十一', '十二', '十三', '十四', '十五', '十六', '十七', '十八', '十九', "二十"][currentClickWeek]

          yield put({
            type: 'setBizData',
            payload: {
              ...fromStorage,
              currentClickWeek,
              currentWeekChinese,
              currentWeek: currentClickWeek
            }
          })
          if (payload) {
            Taro.setNavigationBarTitle({ title: `课表（当前第${currentWeekChinese}周）` })
          }

        } else {
          const { success, data } = yield call(get, '/api/schedule/lesson');
          if (success) {

            let newData = { ...data }
            let schedule = data.scheduleMap
            const { colors } = yield select(state => state.schedule.bizData)
            let hasUsedColor = { key: '' }
            let colorIndex = 0

            // shcedule 结构：shceduleObj[第几周][周几][第几节]
            // {
            //   第几周:{
            //     周几:{
            //       第几节:[
            //         {courseName, room, teacher, weekday},{...}
            //       ]  //采用数组是因为考虑同一时间有多门课
            //     }
            //   }
            // }

            // 第几周
            for (let week in schedule) {
              // 周几
              for (let weekday in schedule[week]) {
                // 第几节
                for (let lesson in schedule[week][weekday]) {
                  newData.scheduleMap[week][weekday][lesson] = schedule[week][weekday][lesson].map(item => {
                    if (!hasUsedColor[item.lessonId]) {
                      hasUsedColor[item.lessonId] = colors[colorIndex % colors.length];
                      colorIndex += 1;
                    }
                    return {
                      ...item,
                      backgroundColor: hasUsedColor[item.lessonId][0],
                      color: hasUsedColor[item.lessonId][1],
                      height: `${item.period.split(',').length * 100}rpx`,
                      marginBottom: `-${item.period.split(',').length * 50}rpx`
                    }
                  })
                }
              }
            }
            console.log(newData)

            newData = {
              ...data,
              lessonData: newData.scheduleMap,
              currentWeek: Math.ceil((new Date().getTime() - new Date(newData.firstDay)) / (3600 * 1000 * 24 * 7)),
              currentWeekChinese: ''
            }

            newData.currentWeekChinese = ['', '一', '二', '三', '四', '五', '六', '七', '八', '九', "十", '十一', '十二', '十三', '十四', '十五', '十六', '十七', '十八', '十九', "二十"][newData.currentWeek]

            yield put({
              type: 'setBizData',
              payload: newData
            })

            Taro.setStorage({
              key: "scheduleData",
              data: newData
            })

            if (payload) {
              Taro.setNavigationBarTitle({ title: `课表（第${newData.currentWeekChinese}周）` })
            }

          }
        }

      } catch (e) {
        console.log('getSchedules', e);
      } finally {
        yield put({
          type: 'setUiData',
          payload: {
            isLoading: false,
          }
        })
      }
    },
    * test({ payload }, { put, call }) {
      console.log(payload);
      try {
        yield put({
          type: 'toggleLoading',
          payload: {
            isLoading: true,
          }
        })
        const success = yield call(DELETE, '/api/user/unBind');
        if (success) {
          Taro.showToast({
            title: '解绑成功,谢谢使用',
            icon: 'success',
            duration: 2000
          })
          Taro.redirectTo({ url: '/pages/login/index' })
        }
      } catch (e) {
        console.log(e);
      } finally {
        yield put({
          type: 'toggleLoading',
          payload: {
            isLoading: false,
          }
        })
      }
    },
  },
  reducers: {
    setUiData(state, { payload }) {
      return {
        ...state,
        uiData: {
          ...state.uiData,
          ...payload,
        }
      };
    },
    setBizData(state, { payload }) {
      return {
        ...state,
        bizData: {
          ...state.bizData,
          ...payload,
        }
      };
    },
  },
};
