import Taro, { Component } from '@tarojs/taro'
import { View, Text } from '@tarojs/components'
import { AtAccordion, AtIcon } from 'taro-ui'
import { connect } from '@tarojs/redux'

import Skeleton from '../../../../components/skeleton'
import CuttingLine from '../../../../components/cutting-line'

import './index.scss'

@connect(({ schedule }) => ({ schedule }))
export default class ItemDetail extends Component {

  config = {
    navigationBarTitleText: '课程详情',
    navigationBarTextStyle: 'white',
    navigationBarBackgroundColor: '#217aff',
  }

  state = {
    rows: {
      1: {
        before: [
          { value: '地点', record: 'room', type: '1' },
          { value: '时间', record: 'period', type: '1' },
        ],
        after: [
          { value: '教师', record: 'teacher', type: '1' },
          { value: '教学周', record: 'weeks', type: '1' },
          { value: '课时', record: 'periodCount', type: '1' },
          { value: '学分', record: 'credit', type: '1' },
          { value: '课程类型', record: 'courseType', type: '1' },
        ]
      },
      2: {
        before: [
          { value: '日期', record: '', type: '1' },
          { value: '时间', record: '', type: '1' },
          { value: '地点', record: '', type: '1' },
        ],
        after: [
          { value: '备注', record: '', type: '1' },
        ]
      }
    }
  }

  componentWillMount() {
    // console.log(this.$router.params)
  }

  handleClick = (index, value) => {
    const { schedule = {}, dispatch } = this.props;
    let { courseDetail } = schedule.bizData;
    courseDetail[index].open = value;
    dispatch({
      type: 'schedule/setBizData',
      payload: { courseDetail }
    })
    // console.log(index, value, courseDetail)
  }

  setSelectLesson = (dict, value) => {
    // console.log(dict, value)
    let scheduleData = Taro.getStorageSync('scheduleData');
    let { lessonData } = scheduleData;

    let currentLesson = lessonData[dict.currentClickWeek][dict.week][dict.lesson]
    // let select = currentLesson.splice(value, 1)
    currentLesson.unshift(currentLesson.splice(value, 1)[0])

    Taro.setStorage({
      key: `lesson${dict.currentClickWeek}${dict.week}${dict.lesson}`,
      data: 0
    })
    Taro.setStorageSync('scheduleData', { ...scheduleData, lessonData })
    this.props.dispatch({
      type: 'schedule/setBizData',
      payload: { lessonData }
    })
    Taro.showModal({
      title: '设置成功',
      content: '该设置仅针对与这一周的这节课生效，不影响其他周',
      showCancel: false,
      success: function () {
        Taro.navigateBack({
          delta: 1
        })
      }
    })
  }

  render() {
    const { rows } = this.state;
    const { params } = this.$router;
    const { schedule = {} } = this.props;
    const { courseDetail } = schedule.bizData;
    const { isSkeleton } = schedule.uiData;
    // console.log(params)
    return (
      <View className='item-detail'>
        <View className='blue-background'></View>
        {
          courseDetail.length < 2 ?
            isSkeleton ? <Skeleton /> :
              <View className='item-detail-card circle-card'>
                <View className='item-detail-card-header'>
                  <View className='item-detail-card-header-name'>{courseDetail[0].courseName}</View>
                </View>
                <View className='item-detail-card-content'>
                  {rows[params.type].before.map(row =>
                    <View className='item-detail-card-content-item' key={row.value}>
                      <View className='item-detail-card-content-item-label'>{row.value}</View>
                      <View className='item-detail-card-content-item-value'>{courseDetail[0][row.record]}</View>
                    </View>)}
                  <CuttingLine />

                  {rows[params.type].after.map(row =>
                    <View className='item-detail-card-content-item' key={row.value}>
                      <View className='item-detail-card-content-item-label'>{row.value}</View>
                      <View className='item-detail-card-content-item-value'>{courseDetail[0][row.record]}</View>
                    </View>)}

                  {/* <View className='item-detail-card-content-item'>
                    <View className='item-detail-card-content-item-label'>共享状态</View>
                    {
                      // isShared
                      true
                        ? <View className='item-detail-card-content-item-value'>
                          <View>共享中，</View>
                          <View className='item-detail-card-content-item-value-link'>结束共享</View>
                        </View>
                        : <View className='item-detail-card-content-item-value'>
                          <View>未共享，</View>
                          <View className='item-detail-card-content-item-value-link'>什么是共享？</View>
                        </View>
                    }
                  </View> */}
                </View>
              </View>
            :
            courseDetail.map((detail, Index) =>
              isSkeleton ? <Skeleton key={detail.courseName} /> :
                <View className='item-detail-card circle-card' key={detail.courseName}>
                  <View className='item-detail-card-header'>
                    <View className='item-detail-card-header-name'>{detail.courseName}</View>
                  </View>
                  <View className='item-detail-card-content'>
                    {rows[params.type].before.map(row =>
                      <View className='item-detail-card-content-item' key={row.value}>
                        <View className='item-detail-card-content-item-label'>{row.value}</View>
                        <View className='item-detail-card-content-item-value'>{courseDetail[Index][row.record]}</View>
                      </View>)}

                    <AtAccordion hasBorder={false} open={detail.open} onClick={this.handleClick.bind(this, Index)}>
                      <CuttingLine />
                      {rows[params.type].after.map(row =>
                        <View className='item-detail-card-content-item' key={row.value}>
                          <View className='item-detail-card-content-item-label'>{row.value}</View>
                          <View className='item-detail-card-content-item-value'>{courseDetail[Index][row.record]}</View>
                        </View>)}
                    </AtAccordion>

                    <View className='item-detail-card-content-item item-detail-card-content-button' onClick={this.setSelectLesson.bind(this, params, Index)} >
                      <AtIcon prefixClass='iconfont' value='swap' size='20' color='#217aff'></AtIcon>
                      <Text className='item-detail-card-content-button-text'>优先在课表展示</Text>
                    </View>
                  </View>
                </View>
            )
        }
      </View>
    )
  }
}