import Taro, { Component } from '@tarojs/taro'
import { View } from '@tarojs/components'
import { connect } from '@tarojs/redux'

import Table from './components/table/index'
import Card from './components/card/index'
import Week from './components/week/index'

import './index.scss'

@connect(({ schedule }) => ({ schedule }))
export default class index extends Component {

  state = {
  }

  componentDidShow() {
  }

  render() {
    return (
      <View className='course'>
        <Week />
        <Table />
        <Card />
      </View>
    )
  }
}
