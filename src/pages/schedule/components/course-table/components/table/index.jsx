import Taro, { Component } from '@tarojs/taro'
import { View } from '@tarojs/components'
import { connect } from '@tarojs/redux'

import './index.scss'

@connect(({ schedule }) => ({ schedule }))
export default class index extends Component {

  weekTurnDate = (day) => {
    const { bizData = {} } = this.props.schedule;
    const { firstDay = '', currentClickWeek=1 } = bizData;
    let nowDate = new Date(firstDay)
    // let oldDate = new Date(firstDay)
    nowDate.setDate(nowDate.getDate() + (currentClickWeek - 1) * 7 + day - 1)
    if(nowDate.getDate()){
      return String(nowDate.getMonth() + 1) + '/' + String(nowDate.getDate())

    } else {
      return '--/--'
    }
  }

  getOneDayLesson = (week, day) => {
    const { bizData = {} } = this.props.schedule
    const { weeks, lessons } = bizData
    // dosth
    if (day == 0) {
      let courses = [[{
        // courseName: currentWeek 表头内容
      }]]
      for (let i = 1; i < lessons.length; i++) {
        courses.push([{
          courseName: i
        }])
      }
      return courses
    }

    // 每一天第一条数据为日期表头，其余初始化为空
    let courses = [[{
      courseName: this.weekTurnDate(day),
      room: weeks[day]
    }]]
    for (let i = 0; i < lessons.length; i++) {
      courses.push('')
    }

    return courses
  }

  render() {
    const { bizData = {} } = this.props.schedule
    let selectLesson = 0;
    const { weeks = [], lessons, currentClickWeek } = bizData

    return (
      <View className='course-table'>
        <View className='course-table-content'>
          {weeks.map((week, Index) => {
            return (
              <View key={week} className={Index == 0 ? 'course-table-content-col' : 'course-table-content-item'}>
                {lessons.map(lesson => {
                  let courses = this.getOneDayLesson(currentClickWeek, Index);
                  let row = lesson == 0 ? week != '' && courses[lesson][selectLesson].courseName.split('/')[0] == new Date().getMonth()+1 && courses[lesson][selectLesson].courseName.split('/')[1] == new Date().getDate() ?
                    'course-table-content-item-row--current' :
                    'course-table-content-item-row' : 'course-table-content-item-card';
                  return (
                    <View
                      className={row}
                      key={lesson}
                    >
                      <View className={row + '-name'}>{courses[lesson][selectLesson].courseName}</View>
                      <View className={row + '-address'}>{courses[lesson][selectLesson].room}</View>
                    </View>
                  )
                })}
              </View>
            )
          })}
        </View>
      </View>
    )
  }
}
