import Taro, { Component } from '@tarojs/taro'
import { Swiper, SwiperItem, View } from '@tarojs/components'
import { connect } from '@tarojs/redux'
import { AtButton, AtIcon } from 'taro-ui'

import './index.scss'

@connect(({ schedule }) => ({ schedule }))
export default class Item extends Component {

  onSwitchWeek = (currentWeek) => {
    // dosth
    this.props.dispatch({
      type: 'schedule/setCurrentWeek',
      payload: currentWeek
    })
  }

  render() {
    // const {week='1', index='0'} = this.props;
    const { schedule = {} } = this.props;
    let { switchWeek = [], currentClickWeek, currentWeek } = schedule.bizData;
    currentClickWeek = Number(currentClickWeek);
    return (
      <View className='course-week'>
        <View className='course-week-left'>
          <AtIcon
            className='course-week-left-icon'
            prefixClass='iconfont'
            onClick={this.onSwitchWeek.bind(this, currentClickWeek < 2 ? 1 : currentClickWeek - 1)}
            value='left' size='20' color='#999'
          />
        </View>
        <Swiper className='course-week-swiper'
          // circular
          current={currentClickWeek < 4 ? 0 : currentClickWeek > 18 ? 15 : currentClickWeek - 3}
          displayMultipleItems={4.7}
        >
          {switchWeek.map(item =>
            <SwiperItem key={item}>
              <AtButton
                key={item}
                className={item == currentWeek ? 'at-button--current' : ''}
                onClick={this.onSwitchWeek.bind(this, item)}
                type={item == currentClickWeek ? 'primary' : 'secondary'}
              >{item}</AtButton>
            </SwiperItem>
          )}
        </Swiper>
        <View className='course-week-right'>
          <AtIcon
            className='course-week-right-icon'
            prefixClass='iconfont'
            onClick={this.onSwitchWeek.bind(this, currentClickWeek > 19 ? 20 : currentClickWeek + 1)}
            value='right' size='20' color='#999'
          />
        </View>
      </View>
    )
  }
}