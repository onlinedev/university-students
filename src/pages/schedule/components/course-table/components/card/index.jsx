import Taro, { Component } from '@tarojs/taro'
import { View } from '@tarojs/components'
import { connect } from '@tarojs/redux'

import './index.scss'

@connect(({ schedule }) => ({ schedule }))
export default class index extends Component {

  detailHandel = (week, lesson) => {
    // dosth
    const { bizData = {} } = this.props.schedule;
    const { lessonData = {}, currentClickWeek } = bizData;
    if(lesson%2 == 0){
      lesson = lesson == 0? 0: lesson-1; 
    }
    if(lessonData[currentClickWeek] && lessonData[currentClickWeek][week]){
      const courses = lessonData[currentClickWeek][week][lesson];
      try {
        let params = `currentClickWeek=${currentClickWeek}&week=${week}&lesson=${lesson}&type=${courses[0].type?1:2}` 
        Taro.showLoading({title:'数据加载中', mask: true})
        this.props.dispatch({
          type: 'schedule/getCourseDetail',
          payload: {scheduleId: ''}
        })
        courses.map(item => {
          this.props.dispatch({
            type: 'schedule/getCourseDetail',
            payload: {
              scheduleId: item.scheduleId,
              courseName: item.courseName,
              currentClickWeek,
              week,
              lesson
            }
          })
        })

        Taro.navigateTo({
          url: '/pages/schedule/components/item-detail/index?' + params
        })
        Taro.hideLoading()
      } catch (e) {
      }
    }
  }

  getOneDayLesson = (day) => {
    const { bizData = {} } = this.props.schedule;
    const { lessonData = {}, currentClickWeek, lessons } = bizData;
    // dosth
    if (day == 0) {
      let courses = [[{
        courseName: ''
      }]]
      for (let i = 1; i < lessons.length; i++) {
        courses.push([{
          courseName: ''
        }])
      }
      return courses
    }

    // 每一天第一条数据为日期表头，其余初始化为空
    let courses = [[{
      courseName: '',
      // room: weeks[day]
    }]]
    for (let i = 0; i < lessons.length; i++) {
      courses.push([{
        courseName: ''
      }])
    }

    try {
      let dayLesson = lessonData[currentClickWeek][day]
      for (let item in dayLesson) {
        courses[item] = dayLesson[item]
      }

    } catch (error) {
      // console.log('getOneDayLesson==>', error)
    }
    return courses
  }

  getSelectLesson = (weekIndex, lesson) => {
    const { bizData = {} } = this.props.schedule;
    const { lessonData = {}, currentClickWeek } = bizData;
    let select = {
      lessons: 1,
      num: 0
    }

    try {
      if(lessonData[currentClickWeek] && lessonData[currentClickWeek][weekIndex] && lessonData[currentClickWeek][weekIndex][lesson]){
  
        if(lessonData[currentClickWeek][weekIndex][lesson].length>1){
          select.lessons = lessonData[currentClickWeek][weekIndex][lesson].length
          const num = Taro.getStorageSync(`lesson${currentClickWeek}${weekIndex}${lesson}`)
          if(num !== ''){
            select.lessons = 1;
            select.num = num;
          }
        }
      }

    } catch (error) {
      console.log('getSelectLesson==>', error)
    }
    return select
  }

  changeStyle = (week, courses, lesson) => {
    let style = [{},{}]
    if (lesson != 0 && week && courses[lesson][0].courseName) {
      // console.log(courses[lesson][0])
      try {
        style[1]['backgroundColor'] = courses[lesson][0].backgroundColor;
        style[1]['color'] = courses[lesson][0].color;
        style[0]['height'] = courses[lesson][0].height;
        style[0]['marginBottom'] = courses[lesson][0].marginBottom;
        // style[0]['z-index'] = '100';
      } catch (error) {
      }
    }
    return style
  }

  render() {
    const { bizData = {} } = this.props.schedule;
    // let selectLesson = 0;
    const { weeks = [], lessons, colorForConflict } = bizData;
    // console.log(this.getOneDayLesson(9,1))

    return (
      <View className='course-card'>
        <View className='course-card-content'>
          {weeks.map((week, weekIndex) => {
            return (
              <View key={week} className={weekIndex == 0 ? 'course-card-content-col' : 'course-card-content-item'}>
                {lessons.map(lesson => {
                  const courses = this.getOneDayLesson(weekIndex);
                  const style = this.changeStyle(week, courses, lesson);
                  const select = this.getSelectLesson(weekIndex, lesson);
                  const row = lesson == 0 ? 'course-card-content-item-row' : 'course-card-content-item-card';
                  return (
                    <View
                      className={row}
                      key={lesson}
                      style={style[0]}
                      onClick={this.detailHandel.bind(this, weekIndex, lesson)}
                    >
                      {select.lessons > 1?
                      <View
                        className={row + '-content'}
                        style={{'backgroundColor': colorForConflict[0], 'color': colorForConflict[1]}}
                      >
                        <View className={row + '-content-name'}>该时段有{select.lessons}门课程</View>
                      </View>
                      :
                      <View
                        className={row + '-content'}
                        style={style[1]}
                      >
                        <View className={row + '-content-name'}>{courses[lesson][0].courseName.slice(0,9)}</View>
                        <View className={row + '-content-address'}>{courses[lesson][0].room}</View>
                      </View>
                      }
                    </View>)
                })}
              </View>
            )
          })}
        </View>
      </View>
    )
  }
}
