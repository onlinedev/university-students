import Taro, { Component } from '@tarojs/taro'
import { View } from '@tarojs/components'
import { connect } from '@tarojs/redux'

import NavBar from '../../components/nav-bar'
import CourseTable from './components/course-table/index'

import './index.scss'

@connect(({ schedule }) => ({ schedule }))
export default class Schedule extends Component {

  config = {
    navigationBarTitleText: '课表',
    navigationBarTextStyle: 'white',
    navigationBarBackgroundColor: '#217aff',
  }

  state = {
    showMenu: false,
    tabList: ['课表', '月历', '事项'],
    menuList: [
      {
        icon: 'plus',
        value: '添加事项',
        handle: { type: 'more', value: 'addSchedule' },
      }, {
        icon: 'sync',
        value: '获取课表',
        handle: { type: 'more', value: 'updateLesson' },
      }, {
        icon: 'setting',
        value: '设置',
        handle: { type: 'url', value: '/pages/me/components/setting/index' },
      }
    ],
    currentTag: 0
  }

  componentDidShow() {
    try {
      this.$scope.getTabBar().setData({
        selected: 0 // 底部导航栏中当前页面对应的index
      })
    } catch (error) {
      console.log(`index:${error}`)
    }

    console.log(this.props.schedule)
    if(this.props.schedule.bizData.lessonData){
      
    } else {
      this.props.dispatch({
        type: 'schedule/getSchedules',
        payload: {isSchedulePage: true}
      })
    }
  }

  addSchedule = () => {
    Taro.showModal({
      title: '提示',
      content: '该功能暂未开放，敬请期待！',
      showCancel: false
    })
  }

  updateLesson = () => {
    const { schedule } = this.props;
    const { updateTimeSpan } = schedule.bizData;
    const timeSpan = Taro.getStorageSync('updateLesson');
    let minutes = (new Date().getHours() - timeSpan[0]) * 60 + new Date().getMinutes() - timeSpan[1];
    if (timeSpan && timeSpan[2]==new Date().getDate() && minutes < updateTimeSpan) {
      Taro.showModal({
        title: '提示',
        content: `先去喝杯咖啡吧，${updateTimeSpan-minutes}分钟后再来试试`,
        showCancel: false
      })
    } else {
        this.props.dispatch({
          type: 'schedule/updateLesson'
        })
    }
  }

  setCurrent = (type, value) => {
    const { bizData = {} } = this.props.schedule
    const { currentWeekChinese } = bizData

    this.setState({
      showMenu: false
    })

    if (type == 'more') {
      this[value]();
    } else if (type == 'url') {
      Taro.navigateTo({ url: value, })
    } else {
      if(value!=0){
        Taro.showModal({
          title: '提示',
          content: '该功能暂未开放，敬请期待！',
          showCancel: false
        })
      }
      this.setState({
        currentTag: value
      })

      Taro.setNavigationBarTitle({
        title: value == 0 ? `${type}（当前第${currentWeekChinese}周）` : type
      })
    }
  }

  showMenuHandle = (value) => {
    this.setState({
      showMenu: value
    })
  }

  render() {
    return (
      <View className='course'>
        <NavBar
          tabList={this.state.tabList}
          menuList={this.state.menuList}
          currentTag={this.state.currentTag}
          onSetCurrent={this.setCurrent}
          showMenu={this.state.showMenu}
          onShowMenuHandle={this.showMenuHandle}
        />
        <CourseTable />
      </View>
    )
  }
}
