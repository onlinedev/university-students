import Taro, { Component } from '@tarojs/taro'
import { View } from '@tarojs/components'

import './index.scss'

export default class ScoreListSkeleton extends Component {

  render() {
    return (
      <View className='score-list-skeleton'>
        <View className='score-list-skeleton-card'>
          <View className='score-list-skeleton-card-title'></View>
          <View className='score-list-skeleton-card-content'>
            <View className='score-list-skeleton-card-content-box'></View>
            <View className='score-list-skeleton-card-content-box'></View>
            <View className='score-list-skeleton-card-content-box'></View>
          </View>
          <View className='score-list-skeleton-card-content'>
            <View className='score-list-skeleton-card-content-box'></View>
            <View className='score-list-skeleton-card-content-box'></View>
            <View className='score-list-skeleton-card-content-box'></View>
          </View>
          <View className='score-list-skeleton-card-content'>
            <View className='score-list-skeleton-card-content-box'></View>
            <View className='score-list-skeleton-card-content-box'></View>
            <View className='score-list-skeleton-card-content-box'></View>
          </View>
          <View className='score-list-skeleton-card-content'>
            <View className='score-list-skeleton-card-content-box'></View>
            <View className='score-list-skeleton-card-content-box'></View>
            <View className='score-list-skeleton-card-content-box'></View>
          </View>
          <View className='score-list-skeleton-card-content'>
            <View className='score-list-skeleton-card-content-box'></View>
            <View className='score-list-skeleton-card-content-box'></View>
            <View className='score-list-skeleton-card-content-box'></View>
          </View>
          <View className='score-list-skeleton-card-content'>
            <View className='score-list-skeleton-card-content-box'></View>
            <View className='score-list-skeleton-card-content-box'></View>
            <View className='score-list-skeleton-card-content-box'></View>
          </View>
        </View>
        <View className='score-list-skeleton-card'>
          <View className='score-list-skeleton-card-title'></View>
        </View>
        <View className='score-list-skeleton-card'>
          <View className='score-list-skeleton-card-title'></View>
        </View>
      </View>
    )
  }
}

