import Taro, { Component } from '@tarojs/taro'
import { View, Text } from '@tarojs/components'
import { AtAccordion, AtIcon } from 'taro-ui'
import { connect } from '@tarojs/redux'

import ScoreListSkeleton from './components/score-list-skeleton/'
import CuttingLine from '../../../../components/cutting-line'

import './index.scss'

@connect(({ score }) => ({ score }))
export default class ScoreList extends Component {

  componentDidMount() {
    const { score = {}, dispatch } = this.props;
    const { scoreList = [] } = score.bizData || {};
    !scoreList.length && dispatch({
      type: 'score/getScoreListData',
    })
  }

  accordionClickHandle = (term) => {
    const { dispatch } = this.props;
    dispatch({
      type: 'score/toggleAccordionOpen',
      payload: term,
    })
  }

  scoreItemClickHandle = (gradeId, fresh, courseCode) => {
    if (fresh < 2) {
      Taro.navigateTo({
        url: `/pages/score/components/score-detail/index?gradeId=${gradeId}`
      })
    } else {
      this.props.dispatch({
        type: 'score/getScoreTeacherData',
        payload: { courseCode, gradeId },
      })
    }
  }

  updateModifiedHandle = (term) => {
    const { score } = this.props;
    const { updateTimeSpan } = score.bizData;
    const timeSpan = Taro.getStorageSync('updateModified');
    let minutes = (new Date().getHours() - timeSpan[0]) * 60 + new Date().getMinutes() - timeSpan[1];
    if (timeSpan && timeSpan[2]==new Date().getDate() && minutes < updateTimeSpan) {
      Taro.showModal({
        title: '提示',
        content: `先去喝杯咖啡吧，${updateTimeSpan - minutes}分钟后再来试试`,
        showCancel: false
      })
    } else {
      this.props.dispatch({
        type: 'score/updateModified',
        payload: { term }
      })
    }
  }

  render() {
    const {
      bizData: {
        scoreList = [],
      },
      uiData: {
        isSkeletonShow,
      }
    } = this.props.score;

    return (
      <View className='score-list'>
        {
          isSkeletonShow
            ? <ScoreListSkeleton />
            : <View>
              {
                scoreList.map(term => {
                  const { termStat, grades } = term
                  const newGrades = grades.map(item => item)
                  const termNum = `${termStat.term.slice(0, 4)}${termStat.term.slice(-2, -1) === '上' ? 1 : 2}`

                  return (
                    <View className='score-list-card' key={termNum}>
                      <View className='score-list-card-header'>
                        {termStat.term.slice(-4, -1)}
                      </View>

                      <View className='score-list-card-profile'>
                        <View className='score-list-card-profile-item'>
                          <View className='score-list-card-profile-item-value'>
                            {Number(termStat.avgScore) ? Number(termStat.avgScore).toFixed(3) : termStat.avgScore}
                          </View>
                          <View className='score-list-card-profile-item-label'>加权均分</View>
                        </View>
                        <View className='score-list-card-profile-item'>
                          <View className='score-list-card-profile-item-value'>
                            {Number(termStat.avgPoint) ? Number(termStat.avgPoint).toFixed(3) : termStat.avgPoint}
                          </View>
                          <View className='score-list-card-profile-item-label'>平均绩点</View>
                        </View>
                        <View className='score-list-card-profile-item'>
                          <View className='score-list-card-profile-item-value'>
                            {Number(termStat.selectionCredits) ? Number(termStat.selectionCredits).toFixed(2) : termStat.selectionCredits}
                          </View>
                          <View className='score-list-card-profile-item-label'>公选学分</View>
                        </View>
                        <View className='score-list-card-profile-item'>
                          <View className='score-list-card-profile-item-value'>
                            <Text>{termStat.rank}</Text>
                            <Text className='score-list-card-profile-item-value-secondary'>
                              /{termStat.rankNum}
                            </Text>
                          </View>
                          <View className='score-list-card-profile-item-label'>专业排名</View>
                        </View>
                      </View>

                      <AtAccordion
                        hasBorder={false}
                        open={termStat.open}
                        onClick={() => { this.accordionClickHandle(termStat.term) }}
                      >
                        <CuttingLine />
                        <View className='score-list-card-content'>
                          <View className='score-list-card-content-item score-list-card-content-title'>
                            <View className='score-list-card-content-item-left'>
                              <View className='score-list-card-content-item-left-name'>
                                课程名称
                              </View>
                            </View>
                            <View className='score-list-card-content-item-right'>
                              <View className='score-list-card-content-item-right-credit'>
                                学分
                              </View>
                              <View className='score-list-card-content-item-right-score'>
                                成绩
                              </View>
                              <View className='score-list-card-content-item-right-icon'>

                              </View>
                            </View>
                          </View>
                          {
                            newGrades.map(item => (
                              <View
                                key={item.gradeId}
                                className='score-list-card-content-item'
                                onClick={this.scoreItemClickHandle.bind(this, item.gradeId, item.fresh, item.courseCode)}
                              >
                                <View className='score-list-card-content-item-left'>
                                  <View className={item.ps || item.fresh ? 'score-list-card-content-item-left-name' : 'score-list-card-content-item-left-name_long'}>
                                    {item.courseName}
                                  </View>

                                  {
                                    item.ps && !item.fresh &&
                                    <View className='score-list-card-content-item-left-tag'>
                                      {item.psType === 1 ? '实体' : ''}
                                      {item.psType === 2 ? '题库' : ''}
                                      {item.psType === 3 ? '视频' : ''}
                                      {item.psType === 4 ? '慕课' : ''}
                                    </View>
                                  }

                                  {
                                    item.fresh &&
                                    <View className='score-list-card-content-item-left-new'>
                                      NEW
                                    </View>
                                  }
                                </View>

                                {
                                  item.fresh === 2 ?
                                    <View className='score-list-card-content-item-right'>
                                      <View className='score-list-card-content-item-right-credit'>
                                      </View>
                                      <View className='score-list-card-content-item-right-score'>
                                        未评教
                                      </View>
                                      <View className='score-list-card-content-item-right-icon'>
                                        <AtIcon value='chevron-right' color='#ccc' />
                                      </View>
                                    </View>
                                    :
                                    <View className='score-list-card-content-item-right'>
                                      <View className='score-list-card-content-item-right-credit'>
                                        {item.credit}
                                      </View>
                                      <View className='score-list-card-content-item-right-score'>
                                        {item.score}
                                      </View>
                                      <View className='score-list-card-content-item-right-icon'>
                                        <AtIcon value='chevron-right' color='#ccc' />
                                      </View>
                                    </View>
                                }
                              </View>
                            ))
                          }
                          <CuttingLine />

                          <View
                            className='score-list-card-content-item score-list-card-content-button'
                            onClick={this.updateModifiedHandle.bind(this, termNum)}
                          >
                            <AtIcon prefixClass='iconfont' value='wrench' size='16' color='#217aff'></AtIcon>
                            <Text className='score-list-card-content-button-text'>修正本学期成绩</Text>
                          </View>
                        </View>
                      </AtAccordion>
                    </View>
                  )
                })
              }
            </View>
        }
      </View>
    )
  }
}
