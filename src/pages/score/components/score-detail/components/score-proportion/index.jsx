import Taro, { Component } from '@tarojs/taro'
import { View } from '@tarojs/components'
import { F2Canvas } from 'taro-f2';
import F2 from "@antv/f2"

import './index.scss'

export default class ScoreComposition extends Component {

  state = {
  }

  drawRadar =  (canvas, width, height) => {
    
    const {
      bizData: {
        selectedScore = {},
      }
    } = this.props.score;

    const {
      gradeDistributions = [],
    } = selectedScore

    const data = gradeDistributions.map(item=>{
      return {
        name:item.name,
        proportion:item.num,
        value:item.value,
        a:'1'}
    })

    const chart = new F2.Chart({
      el: canvas,
      width,
      height
    });
  
    chart.source(data);  

    chart.interval().position('name*proportion').color('#217aff');

    data.map(function(obj) {
      chart.guide().text({
        position: [obj.name, obj.proportion],
        content: obj.value,
        style: {
          textAlign: 'center',
          textBaseline: 'bottom'
        },
        offsetY: -4
      });
    });

    // chart.guide().text({
    //   position:[0,0],
    //   content:'人数',
    //   offsetX: -54,
    //   offsetY: 14,
    // })
    
    chart.render();
  }

  render() {
    return (
        <View className='score-composition'>
          <F2Canvas onCanvasInit={this.drawRadar}></F2Canvas>
        </View>
    )
  }
}