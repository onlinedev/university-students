import Taro, { Component } from '@tarojs/taro'
import { View, Text } from '@tarojs/components'
import { AtIcon, AtAvatar } from 'taro-ui'

import './index.scss'

export default class ScoreShare extends Component {

  shareScoreHandle = () => {
    const { gradeId, dispatch } = this.props;

    Taro.showModal({
      title: '小提示',
      content: '分享之后不可撤销！是否继续分享？',
      success: function (res) {
        if (res.confirm) {
          dispatch({
            type: 'score/scoreDetailShareSubmit',
            payload: gradeId,
          })
        }
      }
    })
  }

  render() {
    const { score = {} } = this.props;
    const { scoreShareData = {} } = score.bizData || {};
    const {
      shareInfos,
      shared,
    } = scoreShareData

    return (
      shared
        ? <View className='score-has-shared'>
          <View className='score-has-shared-title'>成绩排名</View>
          <View className='score-has-shared-content'>
            {
              shareInfos.map((item, index) => (
                <View className='score-has-shared-content-item' key={item.name}>
                  <View className='score-has-shared-content-item-left'>
                    <View className='score-has-shared-content-item-left-num'>
                      {index === 0 ? <AtIcon prefixClass='iconfont' value='trophy-fill' size='24' color='#ffca28' /> : ''}
                      {index === 1 ? <AtIcon prefixClass='iconfont' value='trophy-fill' size='24' color='#c7d8ff' /> : ''}
                      {index === 2 ? <AtIcon prefixClass='iconfont' value='trophy-fill' size='24' color='#f8a77b' /> : ''}
                      {index > 2 ? `${index + 1}` : ''}
                    </View>
                    <AtAvatar circle size='small' image={item.avatar} />
                    <Text className='score-has-shared-content-item-left-name'>
                      {item.name}
                    </Text>
                  </View>
                  <View className='score-has-shared-content-item-right'>
                    {item.score}
                  </View>
                </View>
              ))
            }
          </View>
        </View>
        : <View
          className='score-not-share'
          onClick={this.shareScoreHandle}
        >
          <View className='score-not-share-title'>
            <AtIcon prefixClass='iconfont' value='share' size='20'></AtIcon>
            <Text className='score-not-share-title-text'>分享并查看</Text>
          </View>
          <View className='score-not-share-content'>
            <Text>分享本科成绩和你的微信昵称</Text>
            <Text>并查看其他同学分享的成绩信息</Text>
          </View>
        </View>
    )
  }
}
