import Taro, { Component } from '@tarojs/taro'
import { View, Text } from '@tarojs/components'
import { AtAccordion } from 'taro-ui'
import { connect } from '@tarojs/redux'

import ScoreComposition from './components/score-composition'
import ScoreProportion from './components/score-proportion'
import ScoreShare from './components/score-share';
import Skeleton from '../../../../components/skeleton'
import CuttingLine from '../../../../components/cutting-line'
import ScoreFooter from '../score-footer'

import './index.scss'

@connect(({ score }) => ({ score }))
export default class ScoreDetail extends Component {

  config = {
    navigationBarTitleText: '成绩详情',
    navigationBarTextStyle: 'white',
    navigationBarBackgroundColor: '#217aff',
  }

  state = {
    open: false,
  }

  componentDidMount() {
    const { gradeId } = this.$router.params
    const { dispatch } = this.props;

    dispatch({
      type: 'score/getScoreDetailData',
      payload: gradeId,
    })

    dispatch({
      type: 'score/getScoreShareData',
      payload: gradeId,
    })
  }

  handleClick(value) {
    this.setState({
      open: value
    })
  }

  render() {
    const { score } = this.props;
    const { dispatch } = this.props;
    const { gradeId } = this.$router.params

    const {
      bizData: {
        selectedScore = {},
      },
      uiData: {
        isSkeletonShow,
      }
    } = this.props.score;

    const {
      course = {},
      grade = {},
      gradeStat = {},
      gradeDetails = [],
      gradeDistributions = [],
    } = selectedScore

    const detailInformation = [
      {
        key: '绩点',
        value: !isSkeletonShow ? grade.gradePoint : '---',
      }, {
        key: '学分',
        value: !isSkeletonShow ? grade.credit : '---',
      }, {
        key: '总课时',
        value: course.periodInfo && !isSkeletonShow ? course.periodInfo.total : '---',
      }, {
        key: '课程代码',
        value: !isSkeletonShow ? grade.courseCode : '---',
      }, {
        key: '教学班代码',
        value: !isSkeletonShow ? grade.clazzCode : '---',
      }, {
        key: '开课部门',
        value: !isSkeletonShow ? course.defaultOpenDepart : '---',
      }, {
        key: '课程类型',
        value: !isSkeletonShow ? course.courseTypeName : '---',
      },
    ]

    return (
      <View className='score-detail'>
        <View className='blue-background'></View>
        <View className='score-detail-shade'></View>
        <View className='score-detail-shadow_top'></View>
        <View className='score-detail-shadow_bottom'></View>

        <View>
          <View className='score-detail-card circle-card'>
            <View className='score-detail-card-header'>
              <View className='score-detail-card-header-course'>
                {!isSkeletonShow ? grade.courseName : '---'}
              </View>
              <View className='score-detail-card-header-score'>
                {!isSkeletonShow ? grade.score : '--'}
              </View>
            </View>

            <View className='score-detail-card-profile'>
              <View className='score-detail-card-profile-card'>
                <View className='score-detail-card-profile-card-value'>
                  {Number(gradeStat.topTenScore) ? gradeStat.topTenScore : '--'}
                </View>
                <View className='score-detail-card-profile-card-label'>课程前10%</View>
              </View>
              <View className='score-detail-card-profile-card'>
                <View className='score-detail-card-profile-card-value'>
                  {Number(gradeStat.avgScore) ? Number(gradeStat.avgScore).toFixed(2) : '---'}
                </View>
                <View className='score-detail-card-profile-card-label'>课程平均分</View>
              </View>
              <View className='score-detail-card-profile-card'>
                <View className='score-detail-card-profile-card-value'>
                  <Text>{Number(gradeStat.rank) ? gradeStat.rank : '--'}</Text>
                  <Text className='score-detail-card-profile-card-value-secondary'>
                    /{Number(gradeStat.rankNum) ? gradeStat.rankNum : '--'}
                  </Text>
                </View>
                <View className='score-detail-card-profile-card-label'>课程排名</View>
              </View>
            </View>
            
            <AtAccordion
              hasBorder={false}
              open={this.state.open}
              onClick={this.handleClick.bind(this)}
            >
              <CuttingLine />
              <View className='score-detail-card-more'>
                {
                  detailInformation.map(item => (
                    <View className='score-detail-card-more-item' key={item.key}>
                      <Text className='score-detail-card-more-item-name'>
                        {item.key}
                      </Text>
                      <Text className='score-detail-card-more-item-value'>
                        {item.value}
                      </Text>
                    </View>
                  ))
                }
              </View>
            </AtAccordion>
          </View>
        </View>

        {
          isSkeletonShow
            ? <Skeleton />
            : gradeDetails.length
            && <View className='score-detail-card circle-card'>
              <View className='score-detail-card-title'>成绩明细</View>
              <ScoreComposition score={score} />
            </View>
        }

        {
          isSkeletonShow
            ? <Skeleton />
            : gradeDistributions.length
            && <View className='score-detail-card circle-card'>
              <View className='score-detail-card-title'>成绩分布</View>
              <ScoreProportion score={score} />
            </View>
        }

        {
          isSkeletonShow
            ? <Skeleton />
            : <ScoreShare gradeId={gradeId} score={score} dispatch={dispatch} />
        }

        <ScoreFooter />
      </View>
    )
  }
}
