import Taro, { Component } from '@tarojs/taro'
import { View, } from '@tarojs/components'
import { AtTextarea , AtRadio } from "taro-ui"
import { connect } from '@tarojs/redux'

import CustomButton from '../../../../components/custom-button';
import textJson from '../../../../assets/text.json'

import './index.scss';

@connect(({ score }) => ({ score }))
export default class ScoreListEvaluation extends Component {
  config = {
    navigationBarTitleText: '评教',
    navigationBarTextStyle: 'white',
    navigationBarBackgroundColor: '#217aff',
  }

  state = {
    answers: {
      option1: '1',
      option2: '1',
      option3: '1',
      option4: '1',
      option5: '1',
      option6: '1',
      option7: '1',
      option8: '1',
      option9: '1',
      option10: '1',
    },
  }

  componentDidMount() {
  }

  onOpitonClick = (value, index) => {
    let answers = this.state.answers;
    answers[index] = value;
    this.setState({
      answers
    }, () => {
      // console.log(this.state.answers)
    })
  }

  handleChange(event) {
    this.setState({
      inputValue: event.target.value
    })
  }

  submitForm() {
    const { inputValue = '', answers } = this.state;
    const { evaluationTeacherList = [] } = this.props.score.bizData;

    answers['suggest'] = inputValue;
    answers['lessonSurveyTaskAssoc'] = evaluationTeacherList[0].lessonSurveyTaskAssoc;

    this.props.dispatch({
      type: 'score/evaluationSub',
      payload: answers
    })
  }

  render() {
    const { evaluationTeacherList = [] } = this.props.score.bizData;
    const { isEvaluateLoading } = this.props.score.uiData;
    const evaluation = evaluationTeacherList[0];

    return (
      <View className='score-evaluate'>
        <View className='blue-background'></View>
        <View className='score-evaluate-card circle-card'>
          <View className='score-evaluate-card-header'>
            <View className='score-evaluate-card-header-teacher'>
              {evaluation.teacher}
            </View>
            <View className='score-evaluate-card-header-course'>
              {evaluation.courseName}
            </View>
          </View>

          <View className='score-evaluate-card-content'>

            {textJson.teacherEvaluate.map(item =>
              <View
                className='score-evaluate-card-content-item'
                index={item.index}
                key={item.index}
              >
                <View className='score-evaluate-card-content-item-title'>
                  {item.title}
                </View>
                <AtRadio
                  value={this.state.answers[item.index]}
                  options={item.options}
                  onClick={(value) => this.onOpitonClick(value, item.index)}
                />
              </View>
            )}

            <View className='score-evaluate-card-content-item'>
              <View className='score-evaluate-card-content-item-title'>
                11、其他建议
            </View>
            </View>
            <AtTextarea
              className='score-evaluate-card-content-item-textarea'
              value={this.state.inputValue}
              onChange={this.handleChange}
              maxLength={200}
              placeholder='说点什么吧...'
              placeholderStyle='color:#ccc;'
            />
          </View>
        </View>

        <CustomButton
          isFixed
          type='primary'
          loading={isEvaluateLoading}
          onSubmit={this.submitForm}
        />
      </View>
    )
  }
}
