import Taro, { Component } from '@tarojs/taro'
import { View , Text } from '@tarojs/components'
import { AtIcon } from 'taro-ui'

import './index.scss'

export default class ScoreFooter extends Component {
	render() {
		return (
			<View className='score-footer'>
				<View className='score-footer-title'>
					<AtIcon prefixClass='iconfont' value='info-circle' size='14' color='#999'></AtIcon>
					<Text className='score-footer-title-text'>小提示</Text>
				</View>
				<View className='score-footer-content'>
					成绩信息仅供参考，一切以教务系统为准
				</View>
			</View>
		)
	}
}