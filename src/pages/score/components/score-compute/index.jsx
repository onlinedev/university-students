import Taro, { Component } from '@tarojs/taro'
import { View, Checkbox, Text } from '@tarojs/components'
import { connect } from '@tarojs/redux'
import { AtIcon, AtDrawer, AtButton } from 'taro-ui'

// import PageFooter from "../../../../components/page-footer";

import GpaResult from "./components/gpa-result";

import './index.scss'

@connect(({ score }) => ({ score }))
export default class ScoreCompute extends Component {

  config = {
    navigationBarTitleText: 'GPA计算器',
  }

  state = {
    isFilterDrawerShow: false,
    scoreList: [],
    filterTypeData: [
      {
        type: 1,
        typeName: '公选',
        selected: true,
      },
      {
        type: 2,
        typeName: '其他',
        selected: true,
      },
    ]
  }

  componentDidMount(){
    const { score } = this.props;
    const { scoreList } = score.bizData;
    this.setState({
      scoreList: scoreList.map(item => ({
        ...item,
        selected: true,
        grades: item.grades.map(i => ({ ...i, selected: true })),
      }))
    })
  }

  selectItemHandle = (term, courseCode) => {
    const { scoreList } = this.state;
    this.setState({
      scoreList: scoreList.map(item => (
        item.termStat.term === term
        ? {
          ...item,
            grades: item.grades.map(i => (
              i.courseCode === courseCode
              ? {
                ...i,
                selected: !i.selected,
              }
              : i
            ))
        }
        : item
      ))
    })
  }

  filterTermHandle = term => {
    const { scoreList } = this.state;
    this.setState({
      scoreList: scoreList.map(item => (
        item.termStat.term === term
          ? {
            ...item,
            selected: !item.selected,
            grades: item.grades.map(i => ({
              ...i,
              selected: !item.selected,
            }))
          }
          : item
      ))
    })
  }

  filterTypeHandle = type => {
    const { scoreList, filterTypeData } = this.state;
    const selected = filterTypeData.find(item => item.type === type).selected
    this.setState({
      scoreList: scoreList.map(item => ({
          ...item,
          grades: item.grades.map(i => (
            i.ps === (type === 1)
            ? { ...i, selected: !selected }
            : i
          ))
        })),
      filterTypeData: filterTypeData.map(item => (
        item.type === type
        ?{ ...item, selected: !selected }
        : item
      ))
    })
  }

  render() {
    const { scoreList, isFilterDrawerShow, filterTypeData } = this.state

    return (
      <View className='score-compute'>
        <View className='score-compute-header'>
          <GpaResult scoreList={scoreList} />
        </View>
        <View className='score-compute-term-item score-compute-term-header'>
          <View className='score-compute-term-item-left'>
            <View className='score-compute-term-item-left-box'>课程名称</View>
          </View>
          <View className='score-compute-term-item-right'>
            <View className='score-compute-term-item-right-box'>学分</View>
            <View className='score-compute-term-item-right-box'>成绩</View>
            <AtIcon
              value='filter'
              prefixClass='iconfont'
              className='score-compute-term-item-right-icon'
              onClick={()=>{
                this.setState({ isFilterDrawerShow: true })
              }}
            />
          </View>
        </View>
        {
          scoreList.map(term => (
            <View className='score-compute-term' key={term.termStat.term}>
              <View className='score-compute-term-title'>
                {term.termStat.term}
              </View>
              {
                term.grades.map(item => (
                  <View key={item.courseCode} className='score-compute-term-item' >
                    <View className='score-compute-term-item-left'>
                      <View className='score-compute-term-item-left-box'>
                        {item.courseName}
                      </View>
                      {
                        item.ps && <View className='score-compute-term-item-left-tag'>
                          {item.psType === 1 ? '实体' : ''}
                          {item.psType === 2 ? '试题库' : ''}
                          {item.psType === 3 ? '网络视频' : ''}
                          {item.psType === 4 ? '慕课' : ''}
                        </View>
                      }
                    </View>
                    <View className='score-compute-term-item-right'>
                      <View className='score-compute-term-item-right-box'>
                        {item.credit}
                      </View>
                      <View className='score-compute-term-item-right-box'>
                        {item.score}
                      </View>
                      <Checkbox
                        className='score-compute-term-item-right-checkbox'
                        onClick={this.selectItemHandle.bind(this, term.termStat.term, item.courseCode)}
                        checked={item.selected}
                        color='#77a9fd'
                      />
                    </View>
                  </View>
                ))
              }
            </View>
          ))
        }
        <AtDrawer show={isFilterDrawerShow} right mask width='80%' onClose={() => {
                this.setState({ isFilterDrawerShow: false })
              }}
        >
          <View className='filter-drawer'>
            <Text className='filter-drawer-title'>课程类型</Text>
            <View className='filter-drawer-box'>
              {
                filterTypeData.map(item => (
                  <View
                    key={item.type}
                    className={`${item.selected ? 'filter-drawer-box-tag' : 'filter-drawer-box-noSelect-tag'}`}
                    onClick={this.filterTypeHandle.bind(this, item.type)}
                  >
                    {item.typeName}
                  </View>
                ))
              }
            </View>
            <Text className='filter-drawer-title'>课程学期</Text>
            <View className='filter-drawer-box'>
              {
                scoreList.map(item => (
                  <View
                    key={item.termStat.term}
                    className={`${item.selected ? 'filter-drawer-box-tag' :'filter-drawer-box-noSelect-tag'}`}
                    onClick={this.filterTermHandle.bind(this, item.termStat.term)}
                  >
                    {item.termStat.term}
                  </View>
                ))
              }
            </View>

            <AtButton
              full
              size='small'
              type='primary'
              className='filter-drawer-button'
              onClick={() => {
                this.setState({ isFilterDrawerShow: false })
              }}
            >确定</AtButton>
          </View>
        </AtDrawer>
        {/* <PageFooter /> */}
      </View>
    )
  }
}
