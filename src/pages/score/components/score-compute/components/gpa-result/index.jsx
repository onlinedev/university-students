import Taro, { Component } from '@tarojs/taro'
import { View, Button } from '@tarojs/components'
import { AtIcon, AtModal, AtModalHeader, AtModalContent, AtModalAction } from 'taro-ui'
import { connect } from '@tarojs/redux'

import GpaRule from "./gpa-rule";

import './index.scss'

import {
  getAvgScore,
  USTCFourPointThree,
  standardFour,
  improvementFourOne,
  improvementFourTwo,
  pekingUniversityFour,
  canadaFourPointThree,
  shanghaiFourPointThree,
} from "./gpa";

@connect(({ score }) => ({ score }))
export default class ScoreCompute extends Component {

  state = {
    isGpaRuleShow: false,
    gpaRuleType: ''
  }

  showGpaRuleHandle = gpaRuleType => {
    this.setState({ isGpaRuleShow: true, gpaRuleType })
  }

  render() {
    const { isGpaRuleShow, gpaRuleType } = this.state
    const { scoreList = [] } = this.props

    let gpaData = []

    scoreList.forEach(item => {
      gpaData = [...gpaData, ...item.grades]
    })

    gpaData = gpaData.filter(item => item.selected && Number(item.credit) > 0)

    return (
        <View className='score-compute-result'>

          <View className='score-compute-result-box'>
            <View className='score-compute-result-box-title' style='color:#40a9ff'>加权均分</View>
            <View className='score-compute-result-box-content' style='color:#40a9ff'>{getAvgScore(gpaData).toFixed(4)}</View>
          </View>

          <View className='score-compute-result-box'>
            <View className='score-compute-result-box-title'>
              中科大4.3
                <AtIcon onClick={() => { this.showGpaRuleHandle('中科大4.3') }} value='help' className='score-compute-result-box-title-icon' />
            </View>
            <View className='score-compute-result-box-content'>{USTCFourPointThree(gpaData).toFixed(4)}</View>
          </View>
          <View className='score-compute-result-box'>
            <View className='score-compute-result-box-title'>
              标准4.0
              <AtIcon onClick={() => { this.showGpaRuleHandle('标准4.0') }} value='help' className='score-compute-result-box-title-icon' />
            </View>
          <View className='score-compute-result-box-content'>{standardFour(gpaData).toFixed(4)}</View>
          </View>
          <View className='score-compute-result-box'>
            <View className='score-compute-result-box-title'>
              改进4.0(1)
              <AtIcon onClick={() => { this.showGpaRuleHandle('改进4.0(1)') }} value='help' className='score-compute-result-box-title-icon' />
            </View>
            <View className='score-compute-result-box-content'>{improvementFourOne(gpaData).toFixed(4)}</View>
          </View>
          <View className='score-compute-result-box'>
            <View className='score-compute-result-box-title'>
              改进4.0(2)
              <AtIcon onClick={() => { this.showGpaRuleHandle('改进4.0(2)') }} value='help' className='score-compute-result-box-title-icon' />
            </View>
            <View className='score-compute-result-box-content'>{improvementFourTwo(gpaData).toFixed(4)}</View>
          </View>
          <View className='score-compute-result-box'>
            <View className='score-compute-result-box-title'>
              加拿大4.3
              <AtIcon onClick={() => { this.showGpaRuleHandle('加拿大4.3') }} value='help' className='score-compute-result-box-title-icon' />
            </View>
            <View className='score-compute-result-box-content'>{canadaFourPointThree(gpaData).toFixed(4)}</View>
          </View>
          <View className='score-compute-result-box'>
            <View className='score-compute-result-box-title'>
              上海交大4.3
                <AtIcon onClick={() => { this.showGpaRuleHandle('上海交大4.3') }} value='help' className='score-compute-result-box-title-icon' />
            </View>
            <View className='score-compute-result-box-content'>{shanghaiFourPointThree(gpaData).toFixed(4)}</View>
          </View>
          <View className='score-compute-result-box'>
            <View className='score-compute-result-box-title'>
              北大4.0
                <AtIcon onClick={() => { this.showGpaRuleHandle('北大4.0') }} value='help' className='score-compute-result-box-title-icon' />
            </View>
            <View className='score-compute-result-box-content'>{pekingUniversityFour(gpaData).toFixed(4)}</View>
          </View>

        <AtModal isOpened={isGpaRuleShow}>
          <AtModalHeader>{gpaRuleType}</AtModalHeader>
          <AtModalContent>
            <GpaRule gpaRuleType={gpaRuleType} />
          </AtModalContent>
          <AtModalAction>
            <Button onClick={() => { this.setState({ isGpaRuleShow: false }) }}>确定</Button>
          </AtModalAction>
        </AtModal>
        </View>


    )
  }
}
