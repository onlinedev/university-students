export const getAvgScore = ( scoreList = [] ) => {
  let averageScore = 0,
      totalCredit = 0;

  scoreList.forEach(item => {
    let score = 0;
    if (Number(item.score) === Number(item.score)) {
      score = Number(item.score)
    } else {
      switch (item.score) {
        case '优':  score = 90; break;
        case '良': score = 80; break;
        case '中': score = 70; break;
        case '及格': score = 63; break;
        case '不及格': score = 0; break;
        default: break;
      }
    }
    averageScore = (score * Number(item.credit) + averageScore * totalCredit) / (totalCredit + Number(item.credit))
    totalCredit += Number(item.credit);
  })

  return averageScore;
}


// 中科大4.3 && 本校
export const USTCFourPointThree = ( scoreList = [] ) => {
  let averagePoint = 0,
      totalCredit = 0;

  scoreList.forEach(item => {
    let gradePoint = 0;
    if (Number(item.score) === Number(item.score)) {
      if (Number(item.score) <= 100 && Number(item.score) >= 95) {
        gradePoint = 4.3
      } else if (Number(item.score) <= 94.9 && Number(item.score) >= 90) {
        gradePoint = 4
      } else if (Number(item.score) <= 89.9 && Number(item.score) >= 85) {
        gradePoint = 3.7
      } else if (Number(item.score) <= 84.9 && Number(item.score) >= 82) {
        gradePoint = 3.3
      } else if (Number(item.score) <= 81.9 && Number(item.score) >= 78) {
        gradePoint = 3
      } else if (Number(item.score) <= 77.9 && Number(item.score) >= 75) {
        gradePoint = 2.7
      } else if (Number(item.score) <= 74.9 && Number(item.score) >= 72) {
        gradePoint = 2.3
      } else if (Number(item.score) <= 71.9 && Number(item.score) >= 68) {
        gradePoint = 2
      } else if (Number(item.score) <= 67.9 && Number(item.score) >= 66) {
        gradePoint = 1.7
      } else if (Number(item.score) <= 65.9 && Number(item.score) >= 64) {
        gradePoint = 1.3
      } else if (Number(item.score) <= 63.9 && Number(item.score) >= 60) {
        gradePoint = 1
      } else if (Number(item.score) < 60) {
        gradePoint = 0
      }
    } else {
      switch (item.score) {
        case '优':  gradePoint = 3.9; break;
        case '良': gradePoint = 3; break;
        case '中': gradePoint = 2; break;
        case '及格': gradePoint = 1.2; break;
        case '不及格': gradePoint = 0; break;
        default: break;
      }
    }
    averagePoint = (gradePoint * Number(item.credit) + averagePoint * totalCredit) / (totalCredit + Number(item.credit))
    totalCredit += Number(item.credit);
  })

  return averagePoint;
}

// 标准4.0
export const standardFour = ( scoreList = [] ) => {
  let averagePoint = 0,
    totalCredit = 0;

  scoreList.forEach(item => {
    let gradePoint = 0;
    if (Number(item.score) === Number(item.score)) {
      if (Number(item.score) <= 100 && Number(item.score) >= 90) {
        gradePoint = 4
      } else if (Number(item.score) <= 89.9 && Number(item.score) >= 80) {
        gradePoint = 3
      } else if (Number(item.score) <= 79.9 && Number(item.score) >= 70) {
        gradePoint = 2
      } else if (Number(item.score) <= 69.9 && Number(item.score) >= 60) {
        gradePoint = 1
      } else if (Number(item.score) <= 59.9) {
        gradePoint = 0
      }
    } else {
      switch (item.score) {
        case '优':  gradePoint = 4; break;
        case '良': gradePoint = 3; break;
        case '中': gradePoint = 2; break;
        case '及格': gradePoint = 1; break;
        case '不及格': gradePoint = 0; break;
        default: break;
      }
    }
    averagePoint = (gradePoint * Number(item.credit) + averagePoint * totalCredit) / (totalCredit + Number(item.credit))
    totalCredit += Number(item.credit);
  })

  return averagePoint;
}

// 改进4.0(1)
export const improvementFourOne = ( scoreList = [] ) => {
  let averagePoint = 0,
    totalCredit = 0;

  scoreList.forEach(item => {
    let gradePoint = 0;
    if (Number(item.score) === Number(item.score)) {
      if (Number(item.score) <= 100 && Number(item.score) >= 85) {
        gradePoint = 4
      } else if (Number(item.score) <= 84.9 && Number(item.score) >= 70) {
        gradePoint = 3
      } else if (Number(item.score) <= 69.9 && Number(item.score) >= 60) {
        gradePoint = 2
      }  else if (Number(item.score) <= 59.9) {
        gradePoint = 0
      }
    } else {
      switch (item.score) {
        case '优':  gradePoint = 4; break;
        case '良': gradePoint = 3; break;
        case '中': gradePoint = 2; break;
        case '及格': gradePoint = 1; break;
        case '不及格': gradePoint = 0; break;
        default: break;
      }
    }
    averagePoint = (gradePoint * Number(item.credit) + averagePoint * totalCredit) / (totalCredit + Number(item.credit))
    totalCredit += Number(item.credit);
  })

  return averagePoint;
}

// 改进4.0(2)
export const improvementFourTwo = ( scoreList = [] ) => {
  let averagePoint = 0,
    totalCredit = 0;

  scoreList.forEach(item => {
    let gradePoint = 0;
    if (Number(item.score) === Number(item.score)) {
      if (Number(item.score) <= 100 && Number(item.score) >= 85) {
        gradePoint = 4
      } else if (Number(item.score) <= 84.9 && Number(item.score) >= 75) {
        gradePoint = 3
      } else if (Number(item.score) <= 74.9 && Number(item.score) >= 60) {
        gradePoint = 2
      }  else if (Number(item.score) <= 59.9) {
        gradePoint = 0
      }
    } else {
      switch (item.score) {
        case '优':  gradePoint = 4; break;
        case '良': gradePoint = 3; break;
        case '中': gradePoint = 2; break;
        case '及格': gradePoint = 1; break;
        case '不及格': gradePoint = 0; break;
        default: break;
      }
    }
    averagePoint = (gradePoint * Number(item.credit) + averagePoint * totalCredit) / (totalCredit + Number(item.credit))
    totalCredit += Number(item.credit);
  })

  return averagePoint;
}

// 北大4.0
export const pekingUniversityFour = ( scoreList = [] ) => {
  let averagePoint = 0,
      totalCredit = 0;

  scoreList.forEach(item => {
    let gradePoint = 0;
    if (Number(item.score) === Number(item.score)) {
      if (Number(item.score) <= 100 && Number(item.score) >= 90) {
        gradePoint = 4
      } else if (Number(item.score) <= 89.9 && Number(item.score) >= 85) {
        gradePoint = 3.7
      } else if (Number(item.score) <= 84.9 && Number(item.score) >= 82) {
        gradePoint = 3.3
      } else if (Number(item.score) <= 81.9 && Number(item.score) >= 78) {
        gradePoint = 3
      } else if (Number(item.score) <= 77.9 && Number(item.score) >= 75) {
        gradePoint = 2.7
      } else if (Number(item.score) <= 74.9 && Number(item.score) >= 72) {
        gradePoint = 2.3
      } else if (Number(item.score) <= 71.9 && Number(item.score) >= 68) {
        gradePoint = 2
      } else if (Number(item.score) <= 67.9 && Number(item.score) >= 64) {
        gradePoint = 1.5
      }else if (Number(item.score) <= 63.9 && Number(item.score) >= 60) {
        gradePoint = 1
      } else if (Number(item.score) < 60) {
        gradePoint = 0
      }
    } else {
      switch (item.score) {
        case '优':  gradePoint = 3.9; break;
        case '良': gradePoint = 3; break;
        case '中': gradePoint = 2; break;
        case '及格': gradePoint = 1.2; break;
        case '不及格': gradePoint = 0; break;
        default: break;
      }
    }
    averagePoint = (gradePoint * Number(item.credit) + averagePoint * totalCredit) / (totalCredit + Number(item.credit))
    totalCredit += Number(item.credit);
  })

  return averagePoint;
}

// 加拿大4.3
export const canadaFourPointThree = ( scoreList = [] ) => {
  let averagePoint = 0,
      totalCredit = 0;

  scoreList.forEach(item => {
    let gradePoint = 0;
    if (Number(item.score) === Number(item.score)) {
      if (Number(item.score) <= 100 && Number(item.score) >= 90) {
        gradePoint = 4.3
      } else if (Number(item.score) <= 89.9 && Number(item.score) >= 85) {
        gradePoint = 4
      } else if (Number(item.score) <= 84.9 && Number(item.score) >= 80) {
        gradePoint = 3.7
      } else if (Number(item.score) <= 79.9 && Number(item.score) >= 75) {
        gradePoint = 3.3
      } else if (Number(item.score) <= 74.9 && Number(item.score) >= 70) {
        gradePoint = 3
      } else if (Number(item.score) <= 69.9 && Number(item.score) >= 65) {
        gradePoint = 2.7
      } else if (Number(item.score) <= 64.9 && Number(item.score) >= 60) {
        gradePoint = 2.3
      } else if (Number(item.score) < 60) {
        gradePoint = 0
      }
    } else {
      switch (item.score) {
        case '优':  gradePoint = 3.9; break;
        case '良': gradePoint = 3; break;
        case '中': gradePoint = 2; break;
        case '及格': gradePoint = 1.2; break;
        case '不及格': gradePoint = 0; break;
        default: break;
      }
    }
    averagePoint = (gradePoint * Number(item.credit) + averagePoint * totalCredit) / (totalCredit + Number(item.credit))
    totalCredit += Number(item.credit);
  })

  return averagePoint;
}

// 上海交大4.3
export const shanghaiFourPointThree = ( scoreList = [] ) => {
  let averagePoint = 0,
      totalCredit = 0;

  scoreList.forEach(item => {
    let gradePoint = 0;
    if (Number(item.score) === Number(item.score)) {
      if (Number(item.score) <= 100 && Number(item.score) >= 95) {
        gradePoint = 4.3
      } else if (Number(item.score) <= 94.9 && Number(item.score) >= 90) {
        gradePoint = 4
      } else if (Number(item.score) <= 89.9 && Number(item.score) >= 85) {
        gradePoint = 3.7
      } else if (Number(item.score) <= 84.9 && Number(item.score) >= 80) {
        gradePoint = 3.3
      } else if (Number(item.score) <= 79.9 && Number(item.score) >= 75) {
        gradePoint = 3
      } else if (Number(item.score) <= 74.9 && Number(item.score) >= 70) {
        gradePoint = 2.7
      } else if (Number(item.score) <= 69.9 && Number(item.score) >= 67) {
        gradePoint = 2.3
      } else if (Number(item.score) <= 66.9 && Number(item.score) >= 65) {
        gradePoint = 2
      } else if (Number(item.score) <= 64.9 && Number(item.score) >= 62) {
        gradePoint = 1.7
      } else if (Number(item.score) <= 61.9 && Number(item.score) >= 60) {
        gradePoint = 1
      } else if (Number(item.score) < 60) {
        gradePoint = 0
      }
    } else {
      switch (item.score) {
        case '优':  gradePoint = 3.9; break;
        case '良': gradePoint = 3; break;
        case '中': gradePoint = 2; break;
        case '及格': gradePoint = 1.2; break;
        case '不及格': gradePoint = 0; break;
        default: break;
      }
    }
    averagePoint = (gradePoint * Number(item.credit) + averagePoint * totalCredit) / (totalCredit + Number(item.credit))
    totalCredit += Number(item.credit);
  })

  return averagePoint;
}
