import Taro, { Component } from '@tarojs/taro'
import { View } from '@tarojs/components'

import './index.scss'

export default class GpaRule extends Component {
  state = {
    gradePointRule:[
      {
        key: '中科大4.3',
        rules: [
          { range: '100～95', gradePoint: 4.3 },
          { range: '94.9～90', gradePoint: 4.0 },
          { range: '89.9～85', gradePoint: 3.7 },
          { range: '84.9～82', gradePoint: 3.3 },
          { range: '81.9～78', gradePoint: 3.0 },
          { range: '77.9～75', gradePoint: 2.7 },
          { range: '74.9～72', gradePoint: 2.3 },
          { range: '71.9～68', gradePoint: 2.0 },
          { range: '67.9～65', gradePoint: 1.7 },
          { range: '64.9～64', gradePoint: 1.5 },
          { range: '63.9～61', gradePoint: 1.3 },
          { range: '60.9～60', gradePoint: 1.0 },
          { range: '<60', gradePoint: 0 },
        ]
      }, {
        key: '标准4.0',
        rules: [
          { range: '100～90', gradePoint: 4 },
          { range: '89.9～80', gradePoint: 3 },
          { range: '79.9～70', gradePoint: 2 },
          { range: '69.9～60', gradePoint: 1 },
          { range: '<60', gradePoint: 0 },
        ]
      }, {
        key: '改进4.0(1)',
        rules: [
          { range: '100～85', gradePoint: 4 },
          { range: '84.9～70', gradePoint: 3 },
          { range: '69.9～60', gradePoint: 2 },
          { range: '<60', gradePoint: 0 },
        ]
      }, {
        key: '改进4.0(2)',
        rules: [
          { range: '100～85', gradePoint: 4 },
          { range: '84.9～75', gradePoint: 3 },
          { range: '74.9～60', gradePoint: 2 },
          { range: '<60', gradePoint: 0 },
        ]
      }, {
        key: '北大4.0',
        rules: [
          { range: '100～90', gradePoint: 4 },
          { range: '89.9～85', gradePoint: 3.7 },
          { range: '84.9～82', gradePoint: 3.3 },
          { range: '81.9～78', gradePoint: 3 },
          { range: '77.9～75', gradePoint: 2.7 },
          { range: '74.9～72', gradePoint: 2.3 },
          { range: '71.9～68', gradePoint: 2 },
          { range: '67.9～64', gradePoint: 1.5 },
          { range: '63.9～60', gradePoint: 1 },
          { range: '<60', gradePoint: 0 },
        ]
      }, {
        key: '加拿大4.3',
        rules: [
          { range: '100～90', gradePoint: 4.3 },
          { range: '89.9～85', gradePoint: 4 },
          { range: '84.9～80', gradePoint: 3.7 },
          { range: '79.9～75', gradePoint: 3.3 },
          { range: '74.9～70', gradePoint: 3 },
          { range: '69.9～65', gradePoint: 2.7 },
          { range: '64.9～60', gradePoint: 2.3 },
          { range: '<60', gradePoint: 0 },
        ]
      }, {
        key: '上海交大4.3',
        rules: [
          { range: '100～95', gradePoint: 4.3 },
          { range: '94.9～90', gradePoint: 4 },
          { range: '89.9～85', gradePoint: 3.7 },
          { range: '84.9～80', gradePoint: 3.3 },
          { range: '79.9～75', gradePoint: 3 },
          { range: '74.9～70', gradePoint: 2.7 },
          { range: '69.9～67', gradePoint: 2.3 },
          { range: '66.9～65', gradePoint: 2 },
          { range: '64.9～62', gradePoint: 1.7 },
          { range: '61.9～60', gradePoint: 1 },
          { range: '<60', gradePoint: 0 },
        ]
      },
    ],
  }
  render() {
    const { gradePointRule } = this.state
    const { gpaRuleType } = this.props;

    const gradePointRuleData = gradePointRule.find(item => item.key === gpaRuleType) || {}

    return (
      <View className='score-compute-rule'>
        <View className='score-compute-rule-box score-compute-rule-header'>
          <View className='score-compute-rule-range'>
            成绩
          </View>
          <View className='score-compute-rule-gradePoint'>
            绩点
          </View>
        </View>
        {
          gradePointRuleData.rules.map(item => (
            <View className='score-compute-rule-box' key={item.range}>
              <View className='score-compute-rule-range'>
                {item.range}
              </View>
              <View className='score-compute-rule-gradePoint'>
                {item.gradePoint}
              </View>
            </View>
          ))
        }
      </View>
    )
  }
}
