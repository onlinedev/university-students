import Taro, { Component } from '@tarojs/taro'
import { View, Text } from '@tarojs/components'

import Skeleton from "../../../../../../components/skeleton";

import './index.scss'

export default class PublicElection extends Component {

  state = {
  }

  showPsCourse = (courseCodes, psCourseType) => {
    this.props.dispatch({
      type: 'score/getPsCourseList',
      payload: {
        params: { courseCodes },
        courseType: psCourseType,
        api: '/api/grade/course/code'
      },
    })
  }

  showPsCourseByType = courseType => {
    this.props.dispatch({
      type: 'score/getPsCourseList',
      payload: {
        params: { courseType },
        courseType,
        api: '/api/grade/course/type'
      },
    })
  }

  render() {
    const { score = {} } = this.props;
    const { uiData = {}, bizData = {} } = score
    const { isSkeletonShow } = uiData
    const { courseStats = [], psCourseStats = [] } = bizData.scoreAnalysis || {};

    const electionTypes = [
      courseStats.find(item => item.courseType === 6) || {},
      courseStats.find(item => item.courseType === 7) || {},
      courseStats.find(item => item.courseType === 8) || {},
      courseStats.find(item => item.courseType === 9) || {},
      courseStats.find(item => item.courseType === 10) || {},
      courseStats.find(item => item.courseType === 11) || {},
      courseStats.find(item => item.courseType === 12) || {},
      courseStats.find(item => item.courseType === 13) || {},
      courseStats.find(item => item.courseType === 14) || {},
    ]

    return (
      isSkeletonShow
        ? <Skeleton />
        : <View className='public-election'>
          {
           psCourseStats.length && <View className='public-election-box'>
              <View className='public-election-box-total'>
                已修学分：<Text className='public-election-box-total-num'>
                  {psCourseStats.reduce((tatol, item) => tatol + Number(item.credits), 0)}
                </Text>
              </View>
              <View className='public-election-box-tags'>
                {
                  psCourseStats.map((item, keys) => (
                    <View className='public-election-box-tags-credit' key={'public'+keys} onClick={() => { this.showPsCourse(item.courseCodes, item.psCourseType) }}>
                      <Text className='public-election-box-tags-credit-title'>
                        {item.psCourseType === 1 ? '实体课' : ''}
                        {item.psCourseType === 2 ? '试题库' : ''}
                        {item.psCourseType === 3 ? '视频课' : ''}
                        {item.psCourseType === 4 ? '慕课网' : ''}
                      </Text>
                      <Text className='public-election-box-tags-credit-content'>{item.credits}</Text>
                    </View>
                  ))
                }
              </View>
            </View>
          }
          <View className='public-election-box'>
            <View className='public-election-box-total'>
              已修类目：<Text className='public-election-box-total-num'>
                {electionTypes.filter(item => item.finishCourseNum > 0).length}
              </Text>
            </View>
            <View className='public-election-box-tags'>
              {
                electionTypes.map((item, keys) => item.finishCourseNum > 0
                  ? <View className='public-election-box-tags-type' key={'pubilc'+keys} onClick={() => { this.showPsCourseByType(item.courseType) }}>
                    {item.courseTypeName}
                  </View>
                  : <View className='public-election-box-tags-not-have-type' key={'pubilc'+keys}>
                    {item.courseTypeName}
                  </View>
                )
              }
            </View>
          </View>
        </View>
    )
  }
}
