import Taro, { Component } from '@tarojs/taro'
import { View } from '@tarojs/components'
import { F2Canvas } from 'taro-f2';
import F2 from "@antv/f2"

import Skeleton from "../../../../../../components/skeleton";

import './index.scss'

// import { measureText, fixF2 } from "../../../../../../utils/f2";

// F2.Util.measureText = measureText

export default class ScoreComposition extends Component {

  state = {
  }


  drawRadar =  (canvas, width, height) => {
    const { score = {} } = this.props
    const { gradeDetailStats = [] } = score.bizData.scoreAnalysis;

    // fixF2(F2)
    const chart = new F2.Chart({
      el: canvas,
      width,
      height
    });
    chart.source(gradeDetailStats);
    chart.coord('polar');
    chart.axis('avgScore', {
      grid: {
        lineDash: null
      },
      // label: null,
      line: null
    });
    chart.axis('name', {
      grid: {
        lineDash: null
      }
    });
    chart.tooltip({
      custom: true, // 自定义 tooltip 内容框
      onChange(obj) {
        const legend = chart.get('legendController').legends.top[0];
        const tooltipItems = obj.items;
        const legendItems = legend.items;
        const map = {};
        legendItems.map(item => {
          map[item.name] = Object.assign({}, item);
        });
        tooltipItems.map(item => {
          const { name, avgScore } = item;
          if (map[name]) {
            map[name].avgScore = avgScore;
          }
        });
        legend.setItems(Object.values(map));
      },
      onHide() {
        const legend = chart.get('legendController').legends.top[0];
        legend.setItems(chart.getLegendItems().date);
      }
    });

    chart.area().position('name*avgScore').color('type')
      .animate({
        appear: {
          animation: 'groupWaveIn'
        }
      });
    chart.line().position('name*avgScore').color('type').size(1)
      .animate({
        appear: {
          animation: 'groupWaveIn'
        }
      });
    chart.point().position('name*avgScore').color('type').animate({
      appear: {
        delay: 300
      }
    });
    chart.render();
  }

  render() {
    const { score = {} } = this.props;
    const { isSkeletonShow } = score.uiData || {}
    return (
      isSkeletonShow
        ? <Skeleton />
        : <View className='score-composition'>
          <F2Canvas onCanvasInit={this.drawRadar}></F2Canvas>
        </View>
    )
  }
}
