import Taro, { Component } from '@tarojs/taro'
import { View, Text } from '@tarojs/components'
import { AtTabs, AtTabsPane, AtInput, AtCheckbox, AtButton } from 'taro-ui'
import { connect } from '@tarojs/redux'

// import PageFooter from "../../../../../../components/page-footer";

import './set-score-target.scss'

@connect(({ score }) => ({ score }))
export default class SetScoreTarget extends Component {

  config = {
    navigationBarTitleText: '目标设定',
  }

  state = {
    type: 1,
    criteriaScore: 1,
    criteriagradePoint: 1,
    targetScore: '',
    targetgradePoint: '',
  }

  handleClick(value) {
    this.setState({
      type: value + 1
    })
  }

  changeFormData = (key, value) => {
    this.setState({ [key]: value })
  }

  setTargetHandle = () => {
    const { type, criteriaScore, criteriagradePoint,  targetScore, targetgradePoint } = this.state
    const { scoreAnalysis } = this.props.score.bizData
    const { overGradeStat = {} } = scoreAnalysis;
    const { avgScore, avgCriteriaScore, avgPoint, avgCriteriaPoint } = overGradeStat
    if (type === 1 && (Number(targetScore) !== Number(targetScore) || targetScore > 100 || targetScore < (!criteriaScore ? avgScore : avgCriteriaScore))){
      Taro.showToast({
        icon: "none",
        title: "目标均分设置超出范围",
      });
      return;
    }

    if (type === 2 && (Number(targetgradePoint) !== Number(targetgradePoint) || targetgradePoint > 100 || targetgradePoint < (!criteriagradePoint ? avgPoint : avgCriteriaPoint))) {
      Taro.showToast({
        icon: "none",
        title: "目标绩点设置超出范围",
      });
      return;
    }

    this.props.dispatch({
      type: 'score/scoreTargetDataSubmit',
      payload: {
        type,
        criteria: type === 1 ? criteriaScore : criteriagradePoint,
        target: type === 1 ? targetScore : targetgradePoint,
      },
    })
  }

  render() {
    const { type, criteriaScore, criteriagradePoint, targetScore, targetgradePoint } = this.state;
    const { scoreAnalysis } = this.props.score.bizData
    const { isScoreTargetSubmitButtonLoading } = this.props.score.uiData
    const { overGradeStat = {}, topTenGradeStat = {} } = scoreAnalysis;
    const { avgScore, avgCriteriaScore, avgPoint, avgCriteriaPoint } = topTenGradeStat
    return (
      <View>
      <AtTabs current={type - 1} tabList={[{ title: '平均分设定' }, { title: '绩点设定' }]} onClick={this.handleClick}>
        <AtTabsPane current={type - 1} index={0} >
            <View className='set-target'>
            <View className='set-target-form'>
              <View className='set-target-form-title'>
                目标均分
              </View>
              <AtInput
                clear
                value={targetScore}
                type='digit'
                border={false}
                className='set-target-form-input'
                placeholder={`最低设置 ${!criteriaScore ? overGradeStat.avgScore : overGradeStat.avgCriteriaScore} 分`}
                placeholderStyle='font-size: 25px;'
                onChange={value => { this.changeFormData('targetScore', value) }}
              />
              <View className='set-target-form-note'>
                专业排名 10% 的分数为 {!criteriaScore ? avgScore: avgCriteriaScore}，
                <Text
                  onClick={() => { this.changeFormData('targetScore', !criteriaScore ? avgScore : avgCriteriaScore) }}
                  className='set-target-form-note-updata'
                >快速修改</Text>
                {/* <View className='set-target-form-note-content'>快速修改</View> */}
              </View>
            </View>
            <AtCheckbox
              border={false}
              className='set-target-checkbox'
              selectedList={!criteriaScore ? ['include'] : []}
              options={[{
                value: 'include',
                label: '均分计算是否包含公选',
              }]}
              onChange={() => { this.changeFormData('criteriaScore', criteriaScore ? 0 : 1) }}
            />
              <AtButton
                type='primary'
                className='set-target-button'
                onClick={this.setTargetHandle}
                disabled={isScoreTargetSubmitButtonLoading}
                loading={isScoreTargetSubmitButtonLoading}
              >
              确认
            </AtButton >
            </View>
          </AtTabsPane>
        <AtTabsPane current={type - 1} index={1}>
          <View className='set-target'>
            <View className='set-target-form'>
              <View className='set-target-form-title'>
                目标绩点
              </View>
              <AtInput
                type='digit'
                border={false}
                className='set-target-form-input'
                placeholder={`最低设置 ${!criteriagradePoint ? overGradeStat.avgPoint : overGradeStat.avgCriteriaPoint} 分`}
                placeholderStyle='font-size: 28px;'
                clear
                value={targetgradePoint}
                onChange={value => { this.changeFormData('targetgradePoint', value) }}
              />
              <View className='set-target-form-note'>
                专业排名 10% 的绩点为 {!criteriagradePoint ? avgPoint : avgCriteriaPoint}，
                <Text
                  onClick={() => { this.changeFormData('targetgradePoint', !criteriagradePoint ? avgPoint : avgCriteriaPoint) }}
                  className='set-target-form-note-updata'
                >快速修改</Text>
                {/* <View className='set-target-form-note-content'>快速修改</View> */}
              </View>
            </View>
            <AtCheckbox
              border={false}
              className='set-target-checkbox'
              selectedList={!criteriagradePoint ? ['include'] : []}
              options={[{
                value: 'include',
                label: '绩点计算是否包含公选',
              }]}
              onChange={() => { this.changeFormData('criteriagradePoint', criteriagradePoint ? 0 : 1) }}
            />
              <AtButton
                type='primary'
                className='set-target-button'
                onClick={this.setTargetHandle}
                disabled={isScoreTargetSubmitButtonLoading}
                loading={isScoreTargetSubmitButtonLoading}
              >
              就这样定了！
            </AtButton >
          </View>
          </AtTabsPane>
        </AtTabs>
      {/* <PageFooter /> */}
      </View>
    )
  }
}
