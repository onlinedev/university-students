import Taro, { Component } from '@tarojs/taro'
import { View, Text } from '@tarojs/components'
// import { AtIcon } from 'taro-ui'

import Skeleton from "../../../../../../components/skeleton";

import './index.scss'

export default class ScoreTarget extends Component {

  setTargetHandle = () => {
    Taro.navigateTo({
      url: '/pages/score/components/score-analysis/components/score-target/set-score-target',
    })
  }

  render() {
    const { score = {} } = this.props;
    const { uiData = {}, bizData = {} } = score
    const { isSkeletonShow } = uiData
    const { scoreAnalysis = {} } = bizData
    const { type, hasGoal, mineGrade, target, courseNum, criteria, requireAvgTarget = 0 } = scoreAnalysis.goal || {}

    return (
      isSkeletonShow
        ? <Skeleton />
        : <View className='score-target'>
          {
            !hasGoal
              
              ? <View className='score-target-not-set' onClick={this.setTargetHandle}>
                  <View className='score-target-not-set-title'>
                    + 设定学期目标
                  </View>
                  <View className='score-target-not-set-note'>
                    HFUT online会根据设定的目标和本学期的选课，给出建议
                  </View>
                </View>
              : <View className='score-target-has-set'>
                  <View className='score-target-has-set-header'>
                    <Text className='score-target-has-set-header-title'>我的目标</Text>
                    {/* <AtIcon onClick={this.setTargetHandle} value='edit' color='#888'  size='20' /> */}
                  </View>

                  <View className='score-target-has-set-box'>
                    <View className='score-target-has-set-box-left' onClick={this.setTargetHandle}>
                      <Text className='score-target-has-set-box-left-content'>{Number(target).toFixed(4)}</Text>
                      <Text className='score-target-has-set-box-left-title'>我的目标</Text>
                    </View>
                    <View className='score-target-has-set-box-right'>
                    <Text className='score-target-has-set-box-right-note'>
                      目前
                      <Text>{criteria ? '不' : ''}包含公选</Text>
                      的{type===1 ? '平均分' : '绩点'}为 {mineGrade}
                    </Text>
                      <Text className='score-target-has-set-box-right-content'>
                      需要在未来<Text style='color:#1890ff'>{courseNum}</Text>门课程中{type === 1 ? '分数' : '绩点'}达到
                      <Text style='color:#1890ff'>{requireAvgTarget.toFixed(4)} </Text>以上
                      </Text>
                    </View>
                  </View>
                </View>
          }
        </View>
    )
  }
}
