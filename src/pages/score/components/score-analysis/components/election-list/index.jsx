import Taro, { Component } from '@tarojs/taro'
import { View } from '@tarojs/components'
// import { AtList, AtListItem, AtAccordion } from "taro-ui"
import { connect } from '@tarojs/redux'

import './index.scss'

import Skeleton from "../../../../../../components/skeleton";

@connect(({ score }) => ({ score }))
export default class ElectionList extends Component {

  config = {
    navigationBarTitleText: '成绩 List',
  }

  componentDidMount() {
    const { courseType } = this.$router.params
    let title = '成绩 List'
    switch (Number(courseType)) {
      case 1: title = '实体课'; break;
      case 2: title = '试题库'; break;
      case 3: title = '视频课'; break;
      case 4: title = '慕课网'; break;

      case 6: title = '哲学历史与心理学'; break;
      case 7: title = '文化语言与文学'; break;
      case 8: title = '经济管理与法律'; break;
      case 9: title = '自然环境与科学'; break;
      case 10: title = '信息技术与工程'; break;
      case 11: title = '艺术体育与健康'; break;
      case 12: title = '就业创新与创业'; break;
      case 13: title = '社会交往与礼仪'; break;
      case 14: title = '人生规划品德与修养'; break;
      default: break;
    }
    Taro.setNavigationBarTitle({
      title: title
    })
  }

  render() {
    const { score } = this.props;
    const { psCourseList } = score.bizData;
    const { isSkeletonShow } = score.uiData;

    return (
      <View>
        {
          isSkeletonShow
            ? <View className='election-list-skeleton'><Skeleton /></View>
            : <View className='election-list'>
              <View className='election-list-item election-list-header'>
                <View className='election-list-item-left'>
                  <View className='election-list-item-left-box'>课程</View>
                </View>
                <View className='election-list-item-right'>
                  <View className='election-list-item-right-box'>学分</View>
                  <View className='election-list-item-right-box'>成绩</View>
                </View>
              </View>
              {
                psCourseList.map(item => (
                  <View key={item.courseCode} className='election-list-item'>
                    <View className='election-list-item-left'>
                      <View className='election-list-item-left-box'>
                        {item.courseName}
                      </View>
                    </View>
                    <View className='election-list-item-right'>
                      <View className='election-list-item-right-box'>
                        {item.credit}
                      </View>
                      <View className='election-list-item-right-box'>
                        {item.score}
                      </View>
                    </View>
                  </View>
                ))
              }
            </View>

        }
        {/* <PageFooter /> */}
      </View>
    )
  }
}
