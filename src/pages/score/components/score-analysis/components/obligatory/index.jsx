import Taro, { Component } from '@tarojs/taro'
import { View } from '@tarojs/components'
import { F2Canvas } from 'taro-f2';
import F2 from "@antv/f2"

import Skeleton from "../../../../../../components/skeleton";

import './index.scss'

// import { measureText, fixF2 } from "../../../../../../utils/f2";

// F2.Util.measureText = measureText

export default class Obligatory extends Component {

  componentDidMount(){

  }

  drawRadar = (canvas, width, height) => {
    const { score } = this.props
    const { termStats = [] } = score.bizData.scoreAnalysis;

    // fixF2(F2)
    const chart = new F2.Chart({
      el: canvas,
      width,
      height
    });

    chart.source(termStats.map(item=> ({...item, avgCriteriaScore: Number(item.avgCriteriaScore)})), {
      avgCriteriaScore: {
        // tickCount: 6,
      }
    });

    chart.tooltip({
      custom: true, // 自定义 tooltip 内容框
      // showXTip: true,
      onChange(obj) {
        const legend = chart.get('legendController').legends.top[0];
        const tooltipItems = obj.items;
        const legendItems = legend.items;
        const map = {};
        legendItems.map(item => {
          map[item.name] = Object.assign({}, item);
        });
        tooltipItems.map(item => {
          const { name, avgCriteriaScore } = item;
          if (map[name]) {
            map[name].avgCriteriaScore = avgCriteriaScore;
          }
        });
        legend.setItems(Object.values(map));
      },
      onHide() {
        const legend = chart.get('legendController').legends.top[0];
        legend.setItems(chart.getLegendItems().simpleTerm);
      }
    });

    chart.line().position('simpleTerm*avgCriteriaScore').color('statTypeName').shape('smooth');
    // chart.interval().position('simpleTerm*avgCriteriaScore').color('statTypeName').adjust({
    //   type: 'dodge',
    //   marginRatio: 0.2 // 设置分组间柱子的间距
    // });

    chart.point().position('simpleTerm*avgCriteriaScore').color('statTypeName')
    chart.render();
  }

  render() {
    const { score = {} } = this.props;
    const { isSkeletonShow } = score.uiData || {}
    return (
      isSkeletonShow
      ? <Skeleton />
      : <View className='obligatory'>
        <F2Canvas onCanvasInit={this.drawRadar}></F2Canvas>
      </View>
    )
  }
}
