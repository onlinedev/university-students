import Taro, { Component } from '@tarojs/taro'
import { View, Text } from '@tarojs/components'

import Skeleton from "../../../../../../components/skeleton";

import './index.scss'

export default class ProfessionalElection extends Component {

  render() {
    const { score = {} } = this.props;
    const { uiData = {}, bizData ={} } = score
    const { isSkeletonShow } = uiData;
    const { courseStats = [] } = bizData.scoreAnalysis || {};

    const professionalElection = courseStats.find(item => item.courseTypeName === "专业选修课程")

    const {
      requiredCredits = 0,
      finishCredits = 0,
      learningCredits = 0,
    } = professionalElection || {};

    const unFinishCredits = Number(requiredCredits) - Number(finishCredits) - Number(learningCredits)

    return (
      isSkeletonShow
      ? <Skeleton />
      : <View className='professional-election'>
        <View className='professional-election-all'>
          专业要求总学分：<Text className='professional-election-all-num'>{Number(requiredCredits)}</Text>
        </View>
        <View className='professional-election-progress'>
          {Number(finishCredits) > 0 && <View className='professional-election-progress-completed' style={`flex:${Number(finishCredits)}`} />}
          {Number(learningCredits) > 0 && <View className='professional-election-progress-completing' style={`flex:${Number(learningCredits)}`} />}
          {unFinishCredits > 0 && <View className='professional-election-progress-uncompleted' style={`flex:${unFinishCredits}`} />}
        </View>
        <View className='professional-election-title'>
          {
            Number(finishCredits) > 0 && <View className='professional-election-title-completed' style={`flex:${Number(finishCredits)}`} >
              已修<Text className='professional-election-title-num'>{Number(finishCredits)}学分</Text>
            </View>
          }
          {
            Number(learningCredits) > 0 && <View className='professional-election-title-completing' style={`flex:${Number(learningCredits)}`} >
              在修<Text className='professional-election-title-num'>{Number(learningCredits)}学分</Text>
            </View>
          }
          {
            unFinishCredits > 0 && <View className='professional-election-title-uncompleted' style={`flex:${unFinishCredits}`} >
              未修<Text className='professional-election-title-num'>{unFinishCredits}学分</Text>
            </View>
          }
        </View>
      </View>
    )
  }
}
