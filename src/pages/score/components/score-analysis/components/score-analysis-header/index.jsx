import Taro, { Component } from '@tarojs/taro'
import { View, Text, Button } from '@tarojs/components'
import { AtAvatar } from 'taro-ui'

import './index.scss'

export default class ScoreAnalysisHeader extends Component {


  render() {
    const { score = {} } = this.props;
    const { bizData = {} } = score
    const { userInfo = {}, scoreAnalysis = {} } = bizData;
    const {
      avatarUrl = '',
      nickName = '---',
    } = userInfo;

    const {
      overGradeStat = {},
      student = {},
    } = scoreAnalysis;

    const {
      sno = '---'
    } = student;

    const {
      avgScore = '---',
      avgPoint = '---',
      rankNum = '-',
      rank = '-',
    } = overGradeStat;

    return (
      <View className='score-analysis-header'>
        <Button open-type='share' className='score-analysis-header-note'>
          分享给好友
          </Button>
        <View className='score-analysis-header-user'>
          <AtAvatar size='large' circle image={avatarUrl}></AtAvatar>
          <View className='score-analysis-header-user-content'>
            <Text className='score-analysis-header-user-content-name'>{nickName}</Text>
            <Text className='score-analysis-header-user-content-num'>学号：{sno}</Text>
          </View>
        </View>
        <View className='score-analysis-header-tatol'>
          <View className='score-analysis-header-tatol-box' style='border:0'>
            <Text className='score-analysis-header-tatol-box-content'>{avgScore}</Text>
            <Text className='score-analysis-header-tatol-box-title'>加权平均分</Text>
          </View>
          <View className='score-analysis-header-tatol-box'>
            <Text className='score-analysis-header-tatol-box-content'>{avgPoint}</Text>
            <Text className='score-analysis-header-tatol-box-title'>绩点</Text>
          </View>
          <View className='score-analysis-header-tatol-box'>
            <Text className='score-analysis-header-tatol-box-content'>
              {rank}<Text className='score-analysis-header-tatol-box-content-center'>/ {rankNum}</Text>
            </Text>
            <Text className='score-analysis-header-tatol-box-title'>专业排名</Text>
          </View>
        </View>
      </View>
    )
  }
}
