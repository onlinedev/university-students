import Taro, { Component } from '@tarojs/taro'
import { View, Text } from '@tarojs/components'

import ScoreAnalysisHeader from "./components/score-analysis-header";
import ProfessionalElection from "./components/professional-election/";
import ScoreTarget from "./components/score-target";
import ScoreComposition from "./components/score-composition";
import Obligatory from "./components/obligatory/";
import PublicElection from "./components/public-election";

import './index.scss'

export default class ScoreAnalysis extends Component {

  componentDidMount() {
    // this.props.dispatch({
    //   type: 'score/getDatastamp'
    // })
  }

  render() {
    const { score = {}, dispatch } = this.props;
    // const { isEvaluateView } = score.uiData;

    return (
      <View className='score-analysis'>
        <View className='score-analysis-blue-box' />
        <ScoreAnalysisHeader score={score} />

        {/* <View className='score-analysis-box'>
          <Student score={score} />
        </View> */}
        
        <View className='score-analysis-box'>
          {/* <Text className='score-analysis-box-title'>目标设定</Text> */}
          <ScoreTarget score={score} />
        </View>

        <View className='score-analysis-box'>
          <Text className='score-analysis-box-title'>成绩明细</Text>
          <ScoreComposition score={score} dispatch={dispatch} />
        </View>

        <View className='score-analysis-box'>
          <Text className='score-analysis-box-title'>加权平均分时间走势图</Text>
          <Obligatory score={score} dispatch={dispatch} />
        </View>

        <View className='score-analysis-box'>
          <Text className='score-analysis-box-title'>专业选修课</Text>
          <ProfessionalElection score={score} dispatch={dispatch} />
        </View>

        <View className='score-analysis-box'>
          <Text className='score-analysis-box-title'>公共选修课</Text>
          <PublicElection score={score} dispatch={dispatch} />
        </View>

      </View>
    )
  }
}
