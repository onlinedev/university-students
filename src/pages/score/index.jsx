import Taro, { Component } from '@tarojs/taro'
import { View } from '@tarojs/components'
import { connect } from '@tarojs/redux'

import NavBar from "../../components/nav-bar"
import ScoreAnalysis from "./components/score-analysis"
import ScoreList from "./components/score-list"
import ScoreFooter from "./components/score-footer"

import './index.scss'

@connect(({ score, me, schedule }) => ({ score, me, schedule }))
export default class Score extends Component {

  config = {
    navigationBarTitleText: '成绩',
    navigationBarTextStyle: 'white',
    navigationBarBackgroundColor: '#217aff',
    enablePullDownRefresh: true,
  }

  state = {
    showMenu: 0,
    tabList: ['数据分析', '成绩列表'],
    menuList: [
      {
        icon: 'sync',
        value: '成绩更新',
        handle: { type: 'more', value: 'scoreUpdateHandle' },
      }, {
        icon: 'calculator',
        value: 'GPA计算器',
        handle: { type: 'url', value: '/pages/score/components/score-compute/index' },
      }, {
        icon: 'download',
        value: '成绩下载',
        handle: { type: 'more', value: 'downloadHandle' },
      }, {
        icon: 'setting',
        value: '设置',
        handle: { type: 'url', value: '/pages/me/components/setting/index' },
      }
    ],
    currentTag: 0,
  }

  componentDidShow() {
    try {
      this.$scope.getTabBar().setData({
        selected: 1 // 底部导航栏中当前页面对应的index
      })
    } catch (error) {
      console.log(`index:${error}`)
    }

    this.props.dispatch({
      type: 'score/getDatastamp',
    })

    this.props.dispatch({
      type: 'schedule/getSchedules'
    })
  }

  setCurrent = (type, value) => {
    this.setState({
      showMenu: false
    })
    if (type == 'more') {
      this[value]();
    } else if (type == 'url') {
      Taro.navigateTo({ url: value, })
    } else {
      this.setState({
        currentTag: value
      })

      Taro.setNavigationBarTitle({
        title: type
      })
    }
  }

  showMenuHandle = (value) => {
    this.setState({
      showMenu: value
    })
  }

  onPullDownRefresh() {
    this.setState({
      showMenu: false
    })
    if (this.state.currentTag === 0) {
      this.props.dispatch({
        type: 'score/getDatastamp'
      }).finally(() => {
        Taro.stopPullDownRefresh()
      })
    } else {
      this.props.dispatch({
        type: 'score/getScoreListData',
        notShowSkeleton: true,
      }).finally(() => {
        Taro.stopPullDownRefresh()
      })
    }
  }

  downloadHandle = () => {
    Taro.showLoading({ title: '文件下载中...' })
      .then(() => Taro.getStorage({ key: 'token' }))
      .then(storage => storage.data)
      .then(token => Taro.downloadFile({
        url: 'https://hfutonline.natapp4.cc/api/grade/download',
        header: {
          token
        },
        filePath: wx.env.USER_DATA_PATH + '/成绩单'
      }))
      .then(res => Taro.openDocument({
        filePath: res.filePath,
        fileType: 'xlsx',
      }))
      .then(() => {
        Taro.hideLoading()
      })
      .catch(() => {
        Taro.hideLoading()
      })
  }

  scoreUpdateHandle = () => {
    const { me } = this.props;
    const { updateTimeSpan } = me.bizData;
    const timeSpan = Taro.getStorageSync('updateScoreTime');
    let minutes = (new Date().getHours() - timeSpan[0]) * 60 + new Date().getMinutes() - timeSpan[1];
    if (timeSpan && timeSpan[2]==new Date().getDate() && minutes < updateTimeSpan) {
      Taro.showModal({
        title: '提示',
        content: `先去喝杯咖啡吧，${updateTimeSpan - minutes}分钟后再来试试`,
        showCancel: false
      })
    } else {
      this.props.dispatch({
        type: 'score/updateScore',
        payload: {}
      })
    }
  }

  render() {
    const { score={}, dispatch } = this.props;
    const { currentTag, showMenu } = this.state;

    return (
      <View className='score'>
        <View className='blue-background'></View>
        <NavBar
          tabList={this.state.tabList}
          menuList={this.state.menuList}
          currentTag={this.state.currentTag}
          onSetCurrent={this.setCurrent}
          showMenu={showMenu}
          onShowMenuHandle={this.showMenuHandle}
        />

        {
          currentTag == 0 &&
          <ScoreAnalysis score={score} dispatch={dispatch} />
        }

        {
          currentTag == 1 &&
          <ScoreList score={score} dispatch={dispatch} />
        }

        <ScoreFooter />
      </View>
    )
  }
}
