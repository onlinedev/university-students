import Taro from '@tarojs/taro'
import { get, post } from '../../../utils/request'

export default {
  namespace: 'score',
  state: {
    bizData: {
      userInfo: {},
      scoreAnalysis: {},
      scoreList: [],
      updateTimeSpan: 30,
      selectedScore: {},
      scoreShareData: {},
      psCourseList: [],
      evaluationTeacherList: [],
      clazzStudents: [],
      evaluateStatusMsg: '网络繁忙，请稍后再试'
    },
    uiData: {
      // 全局loading
      isGlobalLoading: false,
      // 互评页loading
      isEvaluateLoading: false,
      // 骨架屏
      isSkeletonShow: false,
      // 评教相关
      isEvaluateStatus: false,
      isEvaluateView: false,
      isEvaluateStatusFinish: false,
      isEvaluateStatusCheck: false,
      token: 'null',
    },
  },
  subscriptions: {
  },
  effects: {
    * getDatastamp(_, { put, call, select }) {
      try {
        let value = Taro.getStorageSync('stamp')
        let scoreData = Taro.getStorageSync('scoreData')

        const { isSettingChange } = yield select(state => state.me.uiData)
        // console.log(isSettingChange)
        const { success, data } = yield call(get, `/api/user/datastamp?stamp=${value}`)
        if (success) {
          if (isSettingChange) {
            yield put({ type: 'getScoreAnalysisData' })
          } else if (data.valid && scoreData) {
            yield put({
              type: 'setBizData',
              payload: { scoreAnalysis: scoreData }
            })
            yield put({ type: 'login/getUserInfo' })
          } else {
            Taro.setStorage({
              key: 'stamp',
              data: data.stamp
            })
            yield put({ type: 'getScoreAnalysisData' })
          }
          console.log('=====', data)

        } else {
          yield put({ type: 'getScoreAnalysisData' })
        }
      } catch (e) {
        // Do something when catch error
      }
    },
    * getScoreAnalysisData({ notShowSkeleton = false }, { call, put }) {
      try {
        if (!notShowSkeleton) {
          yield put({
            type: 'setUiData',
            payload: { isSkeletonShow: true }
          })
        }

        Taro.showLoading({
          title: '获取数据中'
        })

        const { success, data } = yield call(get, '/api/stat/overview')
        if (success && data) {
          yield put({
            type: 'setBizData',
            payload: { scoreAnalysis: data }
          })
          yield put({ type: 'login/getUserInfo' })

          Taro.setStorage({
            key: 'scoreData',
            data
          })

        } else {
          Taro.removeStorage({ key: 'token' })
        }
      } catch (e) {
        yield put({
          type: 'setUiData',
          payload: { isEvaluateStatusCheck: false }
        })
      } finally {
        yield put({
          type: 'setUiData',
          payload: {
            isSkeletonShow: false,
          }
        })

        yield put({
          type: 'me/setUiData',
          payload: { isSettingChange: false }
        })

        Taro.hideLoading()

      }

    },
    * getScoreListData({ notShowSkeleton = false }, { call, put }) {
      try {
        if (!notShowSkeleton) {
          yield put({
            type: 'setUiData',
            payload: { isSkeletonShow: true }
          })
        }
        let fromStorage = yield Taro.getStorage({ key: 'scoreListData' })
          .then(storage => storage.data)
          .catch((e) => console.log(e))

        let pastTime = fromStorage && (new Date().getTime() - fromStorage.updateTime)
        if (pastTime < 72 * 60 * 60 * 1000) {
          yield put({
            type: 'setBizData',
            payload: {
              scoreList: fromStorage.data.map((item, index) => ({
                ...item,
                termStat: {
                  ...item.termStat,
                  open: !index,
                }
              }))
            }
          })
        } else {
          const { success, data } = yield call(get, '/api/stat/grade-list')
          if (success && data) {
            yield put({
              type: 'setBizData',
              payload: {
                scoreList: data.map((item, index) => ({
                  ...item,
                  termStat: {
                    ...item.termStat,
                    open: !index,
                  }
                }))
              }
            })

            Taro.setStorage({
              key: 'scoreListData',
              data: { data: data, updateTime: new Date().getTime() }
            })
          }

        }
      } catch (e) {
      } finally {
        yield put({
          type: 'setUiData',
          payload: { isSkeletonShow: false }
        })
      }

    },
    * getScoreDetailData({ payload, notShowSkeleton = false }, { call, put }) {
      try {
        yield put({
          type: 'setBizData',
          payload: { selectedScore: '' }
        })
        if (!notShowSkeleton) {
          yield put({
            type: 'setUiData',
            payload: { isSkeletonShow: true }
          })
        }
        const { success, data } = yield call(get, `/api/stat/course-grade/${payload}`)
        if (success && data) {
          yield put({
            type: 'setBizData',
            payload: { selectedScore: data }
          })
          yield put({
            type: 'setUiData',
            payload: { isSkeletonShow: false }
          })
        }
      } catch (e) {
      }
    },
    * getScoreShareData({ payload, notShowSkeleton = false }, { put, call }) {
      try {
        if (!notShowSkeleton) {
          yield put({
            type: 'setUiData',
            payload: { isSkeletonShow: true }
          })
        }
        const { success, data } = yield call(get, `/api/grade/share/${payload}`)
        if (success && data) {
          yield put({
            type: 'setBizData',
            payload: { scoreShareData: data }
          })
          yield put({
            type: 'setUiData',
            payload: { isSkeletonShow: false }
          })
        }
      } catch (e) {
      } finally { }
    },
    * scoreDetailShareSubmit({ payload }, { put, call }) {
      try {
        yield put({
          type: 'setUiData',
          payload: {
            isSkeletonShow: true
          }
        })
        const { success, data } = yield call(post, '/api/grade/share', {
          anon: 0,
          gradeId: payload
        })
        if (success && data) {
          yield put({
            type: 'setBizData',
            payload: { scoreShareData: data }
          })
          yield put({
            type: 'setUiData',
            payload: { isSkeletonShow: false }
          })
        }
      } catch (e) {
      } finally { }
    },
    * getPsCourseList({ payload }, { put, call }) {
      try {
        yield call(Taro.navigateTo, {
          url: `/pages/score/components/score-analysis/components/election-list/index?courseType=${payload.courseType}`,
        })
        yield put({
          type: 'setUiData',
          payload: { isSkeletonShow: true }
        })
        const { success, data } = yield call(get, payload.api, payload.params)
        if (success && data) {
          yield put({
            type: 'setBizData',
            payload: { psCourseList: data }
          })
          yield put({
            type: 'setUiData',
            payload: { isSkeletonShow: false }
          })
        }
      } catch (e) {
      } finally { }
    },
    * updateScore({ payload }, { call, put }) {
      Taro.showLoading({ title: '加载中' })
      try {
        yield put({
          type: 'setUiData',
          payload: {
            isGlobalLoading: true
          }
        })
        const { success, data } = yield call(post, '/api/grade/update', payload)
        Taro.hideLoading()
        if (success && data) {
          Taro.setStorage({
            key: "updateScoreTime",
            data: [new Date().getHours(), new Date().getMinutes(), new Date().getDate()]
          })
          Taro.showModal({
            title: '小提示',
            showCancel: false,
            content: '成绩已更新成功，赶快去看看吧',
          })
          try {
            Taro.removeStorageSync('scoreListData')
          } catch (e) {

          }
          yield put({ type: 'getScoreListData' })

        } else {
          Taro.showModal({
            title: '小提示',
            showCancel: false,
            content: '网络情况不良，请您稍后再试',
          })
        }
      } catch (e) {

      } finally {
        yield put({
          type: 'setUiData',
          payload: {
            isGlobalLoading: false
          }
        })
      }

    },
    * updateModified({ payload }, { call, put }) {
      Taro.showLoading({ title: '加载中' })
      try {
        yield put({
          type: 'setUiData',
          payload: {
            isGlobalLoading: true
          }
        })
        const { success, data } = yield call(post, '/api/grade/updateModified', payload)
        Taro.hideLoading()
        if (success && data) {

          Taro.setStorage({
            key: "updateModified",
            data: [new Date().getHours(), new Date().getMinutes(), new Date().getDate()]
          })

          Taro.showModal({
            title: '小提示',
            showCancel: false,
            content: '成绩已修正成功，赶快去看看吧',
          })

          try {
            Taro.removeStorageSync('scoreListData')
          } catch (e) {
          }
          yield put({ type: 'getScoreListData' })

        } else {
          Taro.showModal({
            title: '小提示',
            showCancel: false,
            content: `网络情况不良，请您稍后再来试试吧`,
          })
        }
      } catch (e) {
      } finally {
        yield put({
          type: 'setUiData',
          payload: {
            isGlobalLoading: false
          }
        })
      }

    },
    * scoreTargetDataSubmit({ payload }, { put, call }) {
      try {
        yield put({
          type: 'setUiData',
          payload: {
            isScoreTargetSubmitButtonLoading: true
          }
        })
        const { success, data } = yield call(post, '/api/stat/goal', payload)
        if (success && data) {
          yield put({
            type: 'updateScoreTargetData',
            payload: data
          })
          Taro.navigateBack({ delta: 1 })
        }
      } catch (e) { } finally {
        yield put({
          type: 'setUiData',
          payload: {
            isScoreTargetSubmitButtonLoading: false
          }
        })
      }
    },
    * getScoreTeacherData({ payload }, { put, call, select }) {
      try {
        Taro.showLoading({ title: '加载中' })
        // 保存当前评教课程id 
        const { gradeId } = payload;
        yield put({
          type: 'setUiData',
          payload: { gradeId }
        })
        const { success, data } = yield call(get, '/api/grade/evaluation', payload)
        Taro.hideLoading()
        if (success && data) {

          yield put({
            type: 'setBizData',
            payload: { evaluationTeacherList: data }
          })

          Taro.navigateTo({
            url: `/pages/score/components/score-evaluation/index`
          })

          const { evaluationTeacherList } = yield select(state => state.score.bizData);
          let teacherName = evaluationTeacherList[0].teacher;
          if (evaluationTeacherList.length > 1) {
            teacherName = evaluationTeacherList.reduce((prev, curr) => prev.teacher + '、' + curr.teacher);
          }

          Taro.showModal({
            title: '小提示',
            showCancel: false,
            content: `${evaluationTeacherList[0].courseName}有${teacherName}等${evaluationTeacherList.length}位老师待评教`,
            success: function () { }
          })

        }
      } catch (e) {
        console.log(e);

      } finally {
      }
    },
    * evaluationSub({ payload }, { call, put, select, }) {
      try {
        yield put({
          type: 'setUiData',
          payload: { isEvaluateLoading: true }
        })
        const { success, data, msg } = yield call(post, '/api/grade/evaluation', payload)
        if (success && data) {
          const teachers = yield select(state => state.score.bizData.evaluationTeacherList);
          const gradeId = yield select(state => state.score.uiData.gradeId);

          // 将已评价的老师删除
          const evaluated = teachers.shift()

          if (teachers.length) {
            Taro.showModal({
              title: '小提示',
              content: `${evaluated.teacher}老师评教已完成，还有${teachers.length}位老师待评教，是否继续？`,
              success: function (res) {
                if (res.confirm) {
                  Taro.pageScrollTo({ scrollTop: '0px' })
                } else if (res.cancel) {
                  Taro.switchTab({
                    url: `/pages/score/index`
                  })
                }
              }
            })

            // 更新删了已评价老师列表
            yield put({
              type: 'setBizData',
              payload: { evaluationTeacherList: teachers }
            })
          } else {
            yield put({
              type: 'getScoreListData',
            })
            Taro.showModal({
              title: '小提示',
              content: '当前课程评教已完成！是否需要查看成绩详情？',
              success: function (res) {
                if (res.confirm) {
                  Taro.navigateTo({
                    url: `/pages/score/components/score-detail/index?gradeId=${gradeId}`
                  })
                } else if (res.cancel) {
                  Taro.switchTab({
                    url: `/pages/score/index`
                  })
                }
              }
            })
          }
        } else {
          Taro.showModal({
            title: '小提示',
            showCancel: false,
            content: `${msg}，网络状态不佳，请稍后再试`,
            success: function () {
              Taro.switchTab({
                url: `/pages/score/index`
              })
            }
          })
        }
      } catch (e) {
      } finally {
        yield put({
          type: 'setUiData',
          payload: { isEvaluateLoading: false }
        })
      }

    },
    * evaluationStudentSub({ payload }, { call, put, select }) {
      try {
        yield put({
          type: 'setUiData',
          payload: { isEvaluateLoading: true }
        })
        const { token } = yield select(state => state.score.uiData);
        let pay = '';
        payload.forEach(item => pay = `items=${JSON.stringify(item)}&${pay}`)
        const { success, data, message } = yield call(post, `/evaluate/api/student/evaluate?token=${token}`, pay)
        if (success && data) {
          Taro.showModal({
            title: '小提示',
            showCancel: false,
            content: `互评成功`,
            success: function () {
              Taro.switchTab({
                url: `/pages/score/index`
              })
            }
          })

          yield put({
            type: 'setUiData',
            payload: {
              isEvaluateStatus: false,
              isEvaluateStatusFinish: true
            }
          })
        } else {
          Taro.showModal({
            title: '小提示',
            showCancel: false,
            content: `${message}`,
          })
        }
      } catch (e) {
      } finally {
        yield put({
          type: 'setUiData',
          payload: {
            isEvaluateLoading: false,
            isEvaluateStatus: false
          }
        })
      }

      yield put({
        type: 'setBizData',
        payload: { evaluateStatusMsg: "互评已完成！" }
      })


    },
    * getClazzStudents({ notShowSkeleton = false }, { call, put, select }) {
      try {
        if (!notShowSkeleton) {
          yield put({
            type: 'setUiData',
            payload: { isSkeletonShow: true }
          })
        }
        Taro.showLoading({
          title: '数据加载中'
        })
        const { token } = yield select(state => state.score.uiData);
        let sno;
        try {
          const { scoreAnalysis: {
            student,
          }
          } = yield select(({ score }) => ({ ...score.bizData }));
          sno = student.sno
        } catch (error) {
          console.log("==========getClazz=======", error, "==========")
          const { loginFormData } = yield select(state => state.login.bizData);
          sno = loginFormData.username;
        }
        // let { sno } = scoreAnalysis.student;

        const { success, data } = yield call(get, `/evaluate/api/student/clazz?token=${token}`)
        if (success && data) {
          yield put({
            type: 'setBizData',
            payload: { clazzStudents: data.filter(item => item.sno != sno) }
          })
        }
      } catch (e) {
        console.log("==========getClazz=======", e, "==========")
      } finally {
        yield put({
          type: 'setUiData',
          payload: { isSkeletonShow: false }
        })

        Taro.hideLoading()
      }

    },
    * getevaluateStatus({ payload }, { put }) {
      try {
        yield put({
          type: 'setUiData',
          payload
        })

      } catch (e) {
        console.log(e)
      }
    },
  },
  reducers: {
    toggleAccordionOpen(state, { payload }) {
      const newScoreList = state.bizData.scoreList.map(item => ({
        ...item,
        termStat: item.termStat.term === payload
          ? { ...item.termStat, open: !item.termStat.open }
          : item.termStat
      }))
      return {
        ...state,
        bizData: {
          ...state.bizData,
          scoreList: newScoreList
        }
      };
    },
    updateScoreTargetData(state, { payload }) {
      return {
        ...state,
        bizData: {
          ...state.bizData,
          scoreAnalysis: {
            ...state.bizData.scoreAnalysis,
            goal: payload
          }
        }
      };
    },
    changeUserInfoData(state, { payload }) {
      return {
        ...state,
        bizData: {
          ...state.bizData,
          userInfo: payload
        }
      };
    },
    setUiData(state, { payload }) {
      return {
        ...state,
        uiData: {
          ...state.uiData,
          ...payload,
        }
      };
    },
    setBizData(state, { payload }) {
      return {
        ...state,
        bizData: {
          ...state.bizData,
          ...payload
        }
      };
    },
  },
};
