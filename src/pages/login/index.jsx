import Taro from '@tarojs/taro'
import { connect } from '@tarojs/redux'
import { View, Text, Image } from '@tarojs/components'
import { AtInput, AtIcon } from 'taro-ui'

import CustomButton from '../../components/custom-button'

import './index.scss'

@connect(({ login }) => ({ login }))
export default class Login extends Taro.Component {

  config = {
    navigationStyle: 'custom',
  }

  state = {
    showPwd: false
  }

  componentDidMount = () => {
    const { source = 0 } = this.$router.params
    console.log(source)
  }

  LoginFormDataChangeHandle = (key, value) => {
    const { dispatch } = this.props;
    dispatch({
      type: 'login/changeLoginFormData',
      payload: {
        [key]: value
      }
    })
  }

  submitClickHandle = () => {
    const { dispatch } = this.props;
    dispatch({
      type: 'login/submitLoginFormData'
    })
  }

  forgetPwd = () => {
    Taro.showModal({
      title: '忘记密码？',
      content: '找辅导员！！！',
      showCancel: false,
    })
  }

  agreementPage = () => {
    Taro.navigateTo({
      url: '/pages/login/components/user-agreement/index',
    })
  }

  // submitClickevaluate = () => {
  //   const { dispatch } = this.props;
  //   dispatch({
  //     type: 'login/loginEvaluate'
  //   })
  // }

  render() {
    const { login } = this.props;
    const {
      bizData: {
        loginFormData,
      },
      uiData: {
        isUserLoginButtonDisabled,
        isUserLoginButtonLoading,
      },
    } = login;
    const { showPwd } = this.state;

    return (
      <View className='login'>
        <Image className='login-image'
          src='http://139.198.15.213:10086/hfutonline/login.png'
          mode='aspectFit'
        />

        <View className='login-header'>
          <Text className='login-header-title'>欢迎登录</Text>
          <Text className='login-header-secondary'>请使用信息门户账号登录</Text>
        </View>

        <View className='login-content'>
          <View className='login-content-label'>学号</View>
          <View className='login-content-item'>
            <AtInput
              className='login-content-item-input'
              type='number'
              border={false}
              placeholder='请输入学号'
              placeholder-style='color:#ccc;'
              value={loginFormData.username}
              onChange={(value) => {
                this.LoginFormDataChangeHandle('username', value)
              }}
            />
          </View>

          <View className='login-content-label'>密码</View>
          <View className='login-content-item'>
            <AtInput
              className='login-content-item-input'
              type={showPwd ? '' : 'password'}
              border={false}
              placeholder='请输入信息门户密码'
              placeholder-style='color:#ccc;'
              value={loginFormData.password}
              onChange={(value) => {
                this.LoginFormDataChangeHandle('password', value)
              }}
            />
            <AtIcon
              className='login-content-item-eye'
              prefixClass='iconfont'
              onClick={() => this.setState({ showPwd: !showPwd })}
              value={showPwd ? 'eyeclose-fill' : 'eye'}
              size='20'
              color='#999'
            />
          </View>

          <View
            className='login-content-forget'
            onClick={this.forgetPwd}
          >
            忘记密码
          </View>

          <CustomButton
            value='登录'
            type='primary'
            disabled={isUserLoginButtonDisabled}
            loading={isUserLoginButtonLoading}
            onSubmit={this.submitClickHandle}
          />
        </View>

        <View className='login-footer footer'>
          点击登录即代表你已阅读并同意
          <View
            className='login-footer-link'
            onClick={this.agreementPage}
          >
            《用户协议》
          </View>
        </View>
      </View>
    )
  }
}
