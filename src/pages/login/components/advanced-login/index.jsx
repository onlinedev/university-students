import Taro from '@tarojs/taro'
import { connect } from '@tarojs/redux'
import { View, Text } from '@tarojs/components'
import { AtInput, AtIcon } from 'taro-ui'

import CustomButton from '../../../../components/custom-button'

import './index.scss'

@connect(({ login }) => ({ login }))
export default class AdvancedLogin extends Taro.Component {

  config = {
    navigationStyle: 'custom',
  }

  state = {
    showPwd: false
  }

  componentDidMount = () => {
    const { source = 0 } = this.$router.params
    console.log(source)
  }

  LoginFormDataChangeHandle = (key, value) => {
    const { dispatch } = this.props;
    dispatch({
      type: 'login/changeLoginFormData',
      payload: {
        [key]: value
      }
    })
  }

  submitClickHandle = () => {
    const { dispatch } = this.props;
    dispatch({
      type: 'login/submitLoginFormData'
    })
  }

  agreementPage = () => {
    Taro.navigateTo({
      url: '/pages/login/components/user-agreement/index',
    })
  }

  // submitClickevaluate = () => {
  //   const { dispatch } = this.props;
  //   dispatch({
  //     type: 'login/loginEvaluate'
  //   })
  // }

  render() {
    const { login } = this.props;
    const {
      bizData: {
        loginFormData,
      },
      uiData: {
        isUserLoginButtonDisabled,
        isUserLoginButtonLoading,
      },
    } = login;
    const { showPwd } = this.state;

    return (
      <View className='login'>
        <View className='login-header'>
          <Text className='login-header-title'>高级登录</Text>
          <Text className='login-header-subhead'>请使用信息门户账号及教务系统ID登录</Text>
        </View>

        <View className='login-content'>
          <View className='login-content-label'>学号</View>
          <View className='login-content-item'>
            <AtInput
              className='login-content-item-input'
              type='number'
              border={false}
              placeholder='请输入学号'
              placeholder-style='color:#ccc;'
              value={loginFormData.username}
              onChange={(value) => {
                this.LoginFormDataChangeHandle('username', value)
              }}
            />
          </View>

          <View className='login-content-label'>密码</View>
          <View className='login-content-item'>
            <AtInput
              className='login-content-item-input'
              type={showPwd ? '' : 'password'}
              border={false}
              placeholder='请输入信息门户密码'
              placeholder-style='color:#ccc;'
              value={loginFormData.password}
              onChange={(value) => {
                this.LoginFormDataChangeHandle('password', value)
              }}
            />
            <AtIcon
              className='login-content-item-eye'
              prefixClass='iconfont'
              onClick={() => this.setState({ showPwd: !showPwd })}
              value={showPwd ? 'eyeclose-fill' : 'eye'}
              size='20'
              color='#999'
            />
          </View>

          <View className='login-content-label'>
            <Text>教务系统ID</Text>
            <AtIcon
              className='login-content-label-question'
              prefixClass='iconfont'
              value='question-circle'
              size='20'
              color='#666'
            />
          </View>
          <View className='login-content-item'>
            <AtInput
              className='login-content-item-input'
              type={showPwd ? '' : 'password'}
              border={false}
              placeholder='请输入教务系统ID'
              placeholder-style='color:#ccc;'
              value={loginFormData.password}
              onChange={(value) => {
                this.LoginFormDataChangeHandle('password', value)
              }}
            />
          </View>

          <CustomButton
            value='登录'
            type='primary'
            disabled={!isUserLoginButtonDisabled || isUserLoginButtonLoading}
            loading={isUserLoginButtonLoading}
            onSubmit={this.submitClickHandle}
          />

          <View className='login-footer footer'>
            点击登录即代表你已阅读并同意
            <View
              className='login-footer-link'
              onClick={this.agreementPage}
            >
              《用户协议》
            </View>
          </View>
        </View>
      </View>
    )
  }
}
