import Taro, { Component } from '@tarojs/taro'
import { View, Image } from '@tarojs/components'
import { connect } from '@tarojs/redux'

import './index.scss'

@connect(({ login }) => ({ login }))
export default class PrepareData extends Component {

  config = {
    navigationStyle: 'custom',
  }

  componentDidMount() {
    this.props.dispatch({
      type: 'login/prepareData'
    })
  }

  render() {
    return (
      <View className='prepare-data' style='display:flex; flex-direction:column;'>
        <View className='prepare-data-imgbox'>
          <Image
            src='http://139.198.15.213:10086/hfutonline/data-loading.gif'
            mode='aspectFit'
            className='prepare-data-imgbox-img'
          />
        </View>
        <View className='prepare-data-note'>
            数据加载中，请稍候…
        </View>
      </View>
    )
  }
}
