import Taro, { Component } from '@tarojs/taro'
import { View, Image, Text } from '@tarojs/components'
// import { AtButton } from 'taro-ui'
import { connect } from '@tarojs/redux'

import CustomButton from '../../../../components/custom-button'

import './index.scss'

@connect(({ login }) => ({ login }))
export default class GetUserInfo extends Component {

  config = {
    navigationStyle: 'custom',
  }

  getUserInfoHandle = ({ detail }) => {
    const { userInfo } = detail
    const { dispatch } = this.props
    if (userInfo) {
      dispatch({
        type: 'login/changeUserInfoData',
        payload: userInfo
      })
      dispatch({
        type: 'login/getUserInfo'
      })
      Taro.navigateBack({ delta: 1 })
    } else {
      Taro.showToast({
        icon: "none",
        title: "给个权限呗~",
      });
    }
  }

  render() {
    return (
      <View className='get-user-info'>
        <View className='get-user-info-imgbox'>
          <Image
            src='http://139.198.15.213:10086/hfutonline/logo.png'
            mode='widthFix'
            className='get-user-info-imgbox-img'
          />
        </View>
        <View className='get-user-info-note'>
          <Text className='get-user-info-note-title'>
            HFUTonline申请获得以下权限
          </Text>
          <Text className='get-user-info-note-content'>
            获得你的公开信息(昵称、头像等)
          </Text>
        </View>

        <CustomButton
          value='确认授权'
          type='primary'
          openType='getUserInfo'
          onSubmit={this.getUserInfoHandle}
        />
      </View>
    )
  }
}
