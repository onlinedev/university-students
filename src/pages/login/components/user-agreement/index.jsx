import Taro, { Component } from '@tarojs/taro'
import { View } from '@tarojs/components'

import CuttingLine from '../../../../components/cutting-line'
import textJson from '../../../../assets/text.json'

import './index.scss'

export default class UserAgreement extends Component {
	config = {
		navigationBarTitleText: '用户协议',
		navigationBarTextStyle: 'black',
		navigationBarBackgroundColor: '#fff',
	}

	state = {

	}

	render() {
		return (
			<View className='user-agreement'>
				<View className='user-agreement-overview'>{textJson.userAgreement.overview}</View>
				<CuttingLine />

				{
					textJson.userAgreement.section.map(item =>
						<View className='user-agreement-section' key={item}>
							<View className='user-agreement-section-title'>
								{item.title}
							</View>
							{
								item.content.map(content => {
									let name = content.type === 'semi' ? 'user-agreement-section-content_secondary' : ''
									return (
										<View className={`user-agreement-section-content ${name}`} key={content}>
											{content.value}
										</View>
									)
								})
							}
						</View>
					)
				}

				<CuttingLine />
				<View className='user-agreement-footer'>{textJson.userAgreement.us}</View>
			</View>
		)
	}
}