import Taro from '@tarojs/taro'
import { post, get } from '../../../utils/request'

export default {
  namespace: 'login',
  state: {
    bizData: {
      loginFormData: {
        username: '',
        password: '',
      },
    },
    uiData: {
      isLoginLoading: false,
      isLoginDisabled: false
    },
  },
  subscriptions: { },
  effects: {
    * getUserInfo(_, { put, call }) {
      const { authSetting } = yield call(Taro.getSetting)
      if (authSetting['scope.userInfo']) {
        const { userInfo } = yield call(Taro.getUserInfo)
        yield put({
          type: 'score/changeUserInfoData',
          payload: userInfo
        })
        yield call(post, '/api/user/wechat/user-info', userInfo)
      } else {
        const delay = ms => new Promise(resolve => setTimeout(resolve, ms));
        yield call(delay, 300);

        Taro.navigateTo({
          url: '/pages/login/components/get-user-info/index',
        })
      }
    },
    * submitLoginFormData(_, { call, select, put }) {
        try{
        const {
          bizData: {
            loginFormData,
          },
        } = yield select(({login})=>({...login}))

        yield put({
          type: 'setUiData',
          payload: {
            isLoginLoading: true,
            isLoginDisabled: true
          }
        })
        const  { success, data } = yield call(post, '/api/user/bind', loginFormData);
        if (success) {
          
          // 确保绑定之后重新获取token
          Taro.clearStorage()
          
          if(loginFormData.username=='2015213740'){
            // 小程序审核留后门
            Taro.setStorageSync('loginFormData', loginFormData.username)
          }
          
          const {state } = data
          /** 
           * 0 、2 绑定成功
           * 1  密码验证成功
           * 3  密码不正确
           **/
          if (state === 0 || state === 2) {
            // 跳转tab必须用这个
            Taro.switchTab({
              url: '/pages/score/index'
            })
            Taro.showToast({
              icon: "none",
              title: "绑定成功",
            });
          } else if (state === 1) {
            Taro.redirectTo({
              url: '/pages/login/components/prepare-data/index'
            })
            Taro.showToast({
              icon: "none",
              title: "绑定成功，开始加载数据",
            });
          } else if (state === 3) {
            Taro.showToast({
              icon: "none",
              title: "用户名或密码错误！",
            });
          }
        }
      }catch (e) {

      }finally{
      yield put({
          type: 'setUiData',
          payload: {
            isLoginLoading: false,
            isLoginDisabled: false
          }
        })
      }

    },
    * prepareData(_, { call }) {
      let times = 40;

      const delay = ms => new Promise(resolve => setTimeout(resolve, ms));
      while (times > 0) {
        const { success, data } = yield call(get, '/api/user/date/prepare')
        if(success){
          const {state} = data;
          if(state === 1){
            times -= 1
            yield call(delay, 500); // 延时300ms之后进行下一次的while循环执行
          } else if (state === 2 || state === 0) {
            times = 0
            Taro.showToast({
              icon: "none",
              title: "数据加载完成",
            });
            Taro.switchTab({
              url: '/pages/score/index'
            })
            return;
          } else if (state === 3) {
            times = 0
            Taro.redirectTo({
              url: '/pages/error/index'
            })
            return;
          }
        }else{
          times -= 10
        }
      }
      Taro.showToast({
        icon: "none",
        title: "数据获取失败",
      });
      
      Taro.redirectTo({
        url: '/pages/error/index'
      })
    },
    // * loginEvaluate(_, {put, call, select}){
    //   const {
    //     bizData: {
    //       loginFormData,
    //     },
    //   } = yield select(({login})=>({...login}))

    //   yield put({
    //     type: 'setUiData',
    //     payload: { isLoginLoading: true }
    //   })

    //   const {username, password} = loginFormData;
    //   const res = yield call(post, `/evaluate/api/login/stu/pwd`, {
    //     sno: username,
    //     password
    //   })
    //   if(res.success){
    //     yield put({
    //       type: 'score/getevaluateStatus',
    //       payload: {
    //         token: res.data
    //       }
    //     })
    //     const statusRes = yield call(get, `/evaluate/api/student/evaluate?token=${res.data}`)
    //     if (statusRes.success) {
    //       Taro.navigateTo({
    //         url: '/pages/score/components/student-evaluation/index'
    //       })
    //     } else if(statusRes.code === 2000){
    //       Taro.showToast({
    //         icon: "none",
    //         title: statusRes.message,
    //       });
    //     }
    //   } else if(username == "2015213740" && password== "hfut@2015"){
    //     Taro.showToast({
    //       icon: "none",
    //       title: `当前互评未开启，请等待通知`,
    //     });
    //   } else if(res.code===4008 || res.code === 4007){
    //     Taro.showToast({
    //       icon: "none",
    //       title: `${res.message}。请联系辅导员`,
    //     });
    //   } else {
    //     Taro.showToast({
    //       icon: "none",
    //       title: `当前互评未开启，请等待通知`,
    //     });
    //   }
    //   yield put({
    //     type: 'setUiData',
    //     payload: { isLoginLoading: false }
    //   })
    // }
  },
  reducers: {
    setUiData(state, { payload }){
      return {
        ...state,
        uiData: {
          ...state.uiData,
          ...payload,
        }
      };
    },
    changeLoginFormData(state, { payload }){
      return {
        ...state,
        bizData: {
          ...state.bizData,
          loginFormData:{
            ...state.bizData.loginFormData,
            ...payload,
          }
        },
        uiData: {
          ...state.uiData,
          isLoginDisabled: !(state.bizData.loginFormData.username && state.bizData.loginFormData.password)
        }
      };
    },
    changeUserInfoData(state, { payload }) {
      return {
        ...state,
        bizData: {
          ...state.bizData,
          userInfo: payload
        }
      };
    },
  },
};
