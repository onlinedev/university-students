import Taro, { Component } from '@tarojs/taro'
import { View, Image, Text } from '@tarojs/components'

import './index.scss'

export default class Error extends Component {

  config = {
    navigationStyle: 'custom',
  }

  // entryEvaluation = () =>{
  //   Taro.navigateTo({
  //     url: '/pages/login/index?source=1'
  //   })
  // }

  render() {
    return (
      <View className='error'>
        <Image
          src='https://img.zcool.cn/community/01f9ee5ae19189a801214a61e01a50.png@1280w_1l_2o_100sh.png'
          className='error-img'
        />
        <View className='error-note'>
          <Text className='error-note-title'>
            服务器异常
            </Text>
          <Text className='error-note-content'>
            获取数据时出现问题，请稍后再试
            </Text>
        </View>
      </View>
    )
  }
}
