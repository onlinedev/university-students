import Taro, { Component } from '@tarojs/taro'
import { View } from '@tarojs/components'

import './index.scss'

export default class CuttingLine extends Component {
  render() {
    return (
      <View className='cutting-line'>
        <View className='cutting-line-box'></View>
        <View className='cutting-line-line'></View>
        <View className='cutting-line-box'></View>
      </View>
    )
  }
}