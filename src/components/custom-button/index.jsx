import Taro, { Component } from '@tarojs/taro'
import { AtButton } from 'taro-ui'

import './index.scss'

export default class CustomButton extends Component {

  render() {
    const { value, isFixed, isFull, type, disabled, loading, onSubmit, openType} = this.props;
    const className = `${isFixed ? (isFull ? `fixed-full-button fixed-full-button_${type}` : `fixed-circle-button fixed-circle-button_${type}`)
      : (isFull ? `relative-full-button relative-full-button_${type}` : `relative-circle-button relative-circle-button_${type}`)}`
    // console.log(className)
    // console.log(this.props.isFull)

    return (
      <AtButton
        className={className}
        disabled={disabled}
        loading={loading}
        openType={openType}
        onClick={onSubmit}
        onGetUserInfo={onSubmit}
      >
        {value}
      </AtButton>
    )
  }
}

CustomButton.defaultProps = {
  value: '提交',
  isFixed: false,
  isFull: false,
  type: 'primary',
  disabled: false,
  loading: false,
}
