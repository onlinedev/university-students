import Taro, { Component } from '@tarojs/taro'
import { View } from '@tarojs/components'

import './index.scss'

export default class ScoreDetailSkeleton extends Component {

  render() {
    return (
      <View className='score-detail-skeleton'>
        <View className='score-detail-skeleton-card'>
          <View className='score-detail-skeleton-card-title'></View>
          <View className='score-detail-skeleton-card-content'></View>
          <View className='score-detail-skeleton-card-content'></View>
          <View className='score-detail-skeleton-card-content'></View>
          <View className='score-detail-skeleton-card-content'></View>
        </View>
      </View>
    )
  }
}

